package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.blackcat.currencyedittext.CurrencyEditText;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import it.brindisi.sei.y2016.gruppo1.kiu.R;


/**
 * Created by Matteo on 21/06/2016.
 */
public class FragmentConfirmPlace extends Fragment implements CalendarDatePickerDialogFragment.OnDateSetListener, RadialTimePickerDialogFragment.OnTimeSetListener, View.OnClickListener {

    OnCreateRequestListener listener;

    EditText calendario;
    EditText orario;

    Calendar date = Calendar.getInstance();

    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    private static final String FRAG_TAG_TIME_PICKER = "timePickerDialogFragment";
    private CurrencyEditText fare;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_confirm_place, container, false);

        calendario = (EditText) v.findViewById(R.id.data);
        orario = (EditText) v.findViewById(R.id.ora);
        fare = (CurrencyEditText) v.findViewById(R.id.tariffa);

        date.set(Calendar.HOUR_OF_DAY, 21);
        date.set(Calendar.MINUTE, 0);

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        calendario.setText(df.format(date.getTime()));

        df = DateFormat.getTimeInstance(DateFormat.SHORT);
        orario.setText(df.format(date.getTime()));

        fare.setText(R.string.default_max_fare);

        calendario.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MonthAdapter.CalendarDay minDate = new MonthAdapter.CalendarDay(date);
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(FragmentConfirmPlace.this)
                        .setFirstDayOfWeek(Calendar.SUNDAY)
                        //.setPreselectedDate(towDaysAgo.getYear(), towDaysAgo.getMonthOfYear() - 1, towDaysAgo.getDayOfMonth())
                        .setDateRange(minDate, null)
                        .setDoneText(getString(android.R.string.ok))
                        .setCancelText(getString(android.R.string.cancel));
                cdp.show(getFragmentManager(), FRAG_TAG_DATE_PICKER);
            }
        });

        orario.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                        .setOnTimeSetListener(FragmentConfirmPlace.this)
                        .setDoneText(getString(android.R.string.ok))
                        .setStartTime(date.get(Calendar.HOUR), date.get(Calendar.MINUTE))
                        .setCancelText(getString(android.R.string.cancel));
                rtpd.show(getFragmentManager(), FRAG_TAG_TIME_PICKER);
            }

        });

        Button confirm = (Button) v.findViewById(R.id.confirm);
        confirm.setOnClickListener(this);

        return v;
    }

    public void setListener(OnCreateRequestListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        date.set(Calendar.YEAR, year);
        date.set(Calendar.MONTH, monthOfYear);
        date.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        calendario.setText(df.format(date.getTime()));
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
        date.set(Calendar.MINUTE, minute);
        DateFormat df = DateFormat.getTimeInstance(DateFormat.SHORT);
        orario.setText(df.format(date.getTime()));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.confirm) {
            Date date = this.date.getTime();
            listener.onCreateRequest(date, (int) fare.getRawValue(), "");
        }
    }

    public interface OnCreateRequestListener {
        void onCreateRequest(Date date, int fare, String note);
    }


}
