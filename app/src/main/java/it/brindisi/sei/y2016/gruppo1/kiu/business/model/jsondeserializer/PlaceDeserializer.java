package it.brindisi.sei.y2016.gruppo1.kiu.business.model.jsondeserializer;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Location;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Place;

/**
 * Created by Davide on 10/06/2016.
 */
public class PlaceDeserializer implements JsonDeserializer<Place> {
    @Override
    public Place deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Place p = new Place();
        JsonObject jsonObject = (JsonObject) json;

        //Base
        p.setName(jsonObject.get("name").getAsString());
        if(jsonObject.get("formatted_address")!=null) {
            p.setFormattedAddress(jsonObject.get("formatted_address").getAsString());
        }else{
            p.setFormattedAddress(jsonObject.get("vicinity").getAsString());
        }
        p.setPlaceId(jsonObject.get("place_id").getAsString());
        p.setIcon(jsonObject.get("icon").getAsString());

        //Location
        JsonObject location = jsonObject.getAsJsonObject("geometry").getAsJsonObject("location");
        p.setLocation((Location) context.deserialize(location, Location.class));

        //Photo Reference
        JsonArray photos = jsonObject.getAsJsonArray("photos");
        if(photos != null && photos.size() > 0){
            JsonObject photo = (JsonObject) photos.get(0);
            p.setPhotoReference(photo.get("photo_reference").getAsString());
        }
        return p;
    }
}
