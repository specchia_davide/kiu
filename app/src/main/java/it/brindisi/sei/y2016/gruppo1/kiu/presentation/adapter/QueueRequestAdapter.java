package it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.apache.commons.lang3.time.DurationFormatUtils;

import java.text.DateFormat;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequestStatus;

/**
 * Created by degiu on 21/06/2016.
 */
public class QueueRequestAdapter extends DatabaseBaseAdapter<QueueRequest> {
    private Context context;

    public QueueRequestAdapter(Context context) {
        this.context = context;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if (v == null) {
            v = LayoutInflater.from(context).inflate(R.layout.queue_request_list_item, null);
        }
        QueueRequest request = (QueueRequest) this.getItem(position);
        TextView name = (TextView) v.findViewById(R.id.place_name);
        name.setText(request.getPlace().getName());
        switch (request.getStatus()) {
            case WAITING_FOR_QUEUE: {
                v.findViewById(R.id.date_container).setVisibility(View.VISIBLE);
                TextView date = (TextView) v.findViewById(R.id.date);
                date.setText(DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(request.getStartTime()));
                break;
            }
            case ACTIVE: {
                int count = request.getQueue();
                v.findViewById(R.id.ongoing_container).setVisibility(View.VISIBLE);
                TextView time_estimated = (TextView) v.findViewById(R.id.tempo_stimato);
                time_estimated.setText(DurationFormatUtils.formatDuration(request.getQueue() * 15 * 60000, "HH'h' mm'm'"));
                TextView people_queue = (TextView) v.findViewById(R.id.persone_in_coda);

                String people_queue_text= (request.getQueue() + " " + context.getResources().getQuantityString(R.plurals.people_left, count));
                SpannableStringBuilder builder = new SpannableStringBuilder();
                SpannableString colorSpannable= new SpannableString(people_queue_text);
                if(request.getQueue() > 2) {
                    colorSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#43B854")), 0, people_queue_text.length(), 0);
                    builder.append(colorSpannable);
                }else if (count >= 1 || count <= 2){
                    colorSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#FF9800")), 0, people_queue_text.length(), 0);
                    builder.append(colorSpannable);
                }

                people_queue.setText(builder, TextView.BufferType.SPANNABLE);

                break;
            }
            case WAITING_FOR_CLOSE: {
                v.findViewById(R.id.terminate).setVisibility(View.VISIBLE);
                break;
            }
            case NO_HELPER_SELECTED:
                v.findViewById(R.id.no_helper_selected_container).setVisibility(View.VISIBLE);
                break;
        }
        return v;
    }
}
