package it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by Davide on 02/07/2016.
 */
public class HelperAdapter extends BaseAdapter {
    private List<User> helpers;
    private Context context;

    public HelperAdapter(List<User> helpers, Context context) {
        this.helpers = helpers;
        this.context = context;
    }

    @Override
    public int getCount() {
        return helpers.size();
    }

    @Override
    public Object getItem(int position) {
        return helpers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v==null){
            v = LayoutInflater.from(context).inflate(R.layout.helper_list_item, null);
        }
        User h = this.helpers.get(position);
        ImageView profilePic = (ImageView) v.findViewById(R.id.profilePic);
        Picasso.with(context).load(h.getPhotoUrl()).placeholder(R.drawable.user_avatar).transform(new CropCircleTransformation()).into(profilePic);
        TextView name = (TextView) v.findViewById(R.id.name);
        name.setText(h.getName());
        TextView fare = (TextView) v.findViewById(R.id.fare);
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        fare.setText(nf.format(h.getHelperSettings().getFare() / 100f));
        RatingBar ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);
        ratingBar.setProgress(h.getHelperRating());
        return v;
    }
}
