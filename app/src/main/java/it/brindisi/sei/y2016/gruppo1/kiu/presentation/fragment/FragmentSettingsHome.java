package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.app.Fragment;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;

public class FragmentSettingsHome extends android.support.v4.app.Fragment{

    ListView preferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ApplicationServiceUser app_user=new ApplicationServiceUser();
        User user;
        List<String> strings_preferences=new ArrayList<>();

        View v = inflater.inflate(R.layout.fragment_settings_home, container, false);

        preferences=(ListView) v.findViewById(R.id.preferenceslist);
        user = app_user.getLoggedUser();
        strings_preferences.add(getString(R.string.settings_kiuer));
        if(user.isHelper())
            strings_preferences.add(getString(R.string.settings_helper));
        ListAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.settings_home_list_item, R.id.preference, strings_preferences);
        preferences.setAdapter(adapter);

        AdapterView.OnItemClickListener clickListener = new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view,
                                    int position, long id) {
                if (position == 0) {
                    FragmentSettingsKiuer fragment_kiuer = new FragmentSettingsKiuer();
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container, fragment_kiuer)
                            .addToBackStack(null)
                            .commit();
                }
                else{
                    if(position==1){
                        FragmentSettingsHelper fragment_helper = new FragmentSettingsHelper();
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, fragment_helper)
                                .addToBackStack(null)
                                .commit();
                    }
                }
            }
        };
        preferences.setOnItemClickListener(clickListener);

        return v;
    }

}
