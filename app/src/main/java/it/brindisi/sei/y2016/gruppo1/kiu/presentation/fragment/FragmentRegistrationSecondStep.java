package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import it.brindisi.sei.y2016.gruppo1.kiu.R;

/**
 * Created by Matteo on 30/06/2016.
 */
public class FragmentRegistrationSecondStep extends Fragment implements View.OnClickListener {

    private RegistrationSecondStepListener listener;
    private LinearLayout upLinear;
    private LinearLayout downLinear;

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.upLinear:
                AlertDialog.Builder alertKiuer = new AlertDialog.Builder(getActivity());
                alertKiuer.setMessage(getString(R.string.conferma_kiuer));
                        alertKiuer.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                listener.openHomeKiuer();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().getSupportFragmentManager().popBackStackImmediate();
                            }
                        });
                AlertDialog alert_kiu = alertKiuer.create();
                alert_kiu.show();

                break;
            case R.id.downLinear:
                AlertDialog.Builder alertHelper = new AlertDialog.Builder(getActivity());
                alertHelper.setMessage(getString(R.string.conferma_kiuer));
                alertHelper.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        listener.openHomeHelper();                    }
                })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().getSupportFragmentManager().popBackStackImmediate();
                            }
                        });
                AlertDialog alert_help = alertHelper.create();
                alert_help.show();

                break;
        }
    }

    public void setListener(RegistrationSecondStepListener listener) {
        this.listener = listener;
    }

    public void onBackPressed() {
    }

    public interface RegistrationSecondStepListener{
        void openHomeKiuer();
        void openHomeHelper();
    }


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_registration_second_step, container, false);

        upLinear = (LinearLayout) v.findViewById(R.id.upLinear);
        upLinear.setOnClickListener(this);

        downLinear = (LinearLayout) v.findViewById(R.id.downLinear);
        downLinear.setOnClickListener(this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        return v;
    }


}
