package it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.network;

import android.os.AsyncTask;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import it.brindisi.sei.y2016.gruppo1.kiu.business.model.PlaceResult;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.Constants;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

/**
 * Created by Davide on 08/06/2016.
 */
public class AsyncTaskSearchPlace extends AsyncTask<String, Void, String>{

    private static final String PLACE_KEY = Constants.GOOGLE_API_KEY;
    private final String PLACE_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json?key="+PLACE_KEY+"&query=";
    private final OnCompleteListener<String> onCompleteListener;

    public AsyncTaskSearchPlace(OnCompleteListener<String> onCompleteListener) {
        this.onCompleteListener = onCompleteListener;
    }

    @Override
    protected void onPostExecute(String s) {
        this.onCompleteListener.onComplete(true, s);
    }

    @Override
    protected String doInBackground(String... params) {
        String query = params[0].replace(" ", "+");
        HttpRequest request = HttpRequest.get(PLACE_URL + query);
        String json = request.body();
        return json;
    }
}
