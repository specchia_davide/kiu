package it.brindisi.sei.y2016.gruppo1.kiu.presentation.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.Paint;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider.ManageQueueCardProvider;

/**
 * Created by Davide on 06/07/2016.
 */
public class QueueButton extends View {
    private State state;
    private int lastBallDegree = 120;

    private Paint backgroundPaint;
    private Paint animatedBallPaint;
    private Paint textPaint;

    Handler mHandler = new Handler();
    Runnable mTick = new Runnable() {
        public void run() {
            invalidate();
            mHandler.postDelayed(this, 20); // 20ms == 60fps
        }
    };
    private ManageQueueCardProvider.OnQueueListener listener;
    private Paint backgroundPaintStopped;
    private int peopleLeft;

    public QueueButton(Context context) {
        super(context);
        init();
    }

    public QueueButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int radius =  getWidth() / 2 - getPaddingRight();
        if(state == State.STOPPED){
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius, this.backgroundPaintStopped);
            canvas.drawText(getContext().getString(R.string.ended_queue), getWidth() / 2, getHeight() / 2 + 25, textPaint);
        }else{
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius, this.backgroundPaint);
            if(state == State.IDLE) {
                canvas.drawText(getContext().getString(R.string.start), getWidth() / 2, getHeight() / 2 + 25, textPaint);
            }else{
                canvas.drawText(getContext().getString(R.string.queue_next), getWidth() / 2, getHeight() / 2, textPaint);
                canvas.drawText(String.valueOf(peopleLeft), getWidth() / 2, getHeight() / 2 + 55, textPaint);
                radius -= 20;
                canvas.drawCircle(getWidth() / 2 + (float)Math.cos(Math.toRadians(lastBallDegree)) * radius, getHeight() / 2 + (float) Math.sin(Math.toRadians(lastBallDegree)) * radius, 10, this.animatedBallPaint);
                lastBallDegree = (++lastBallDegree) % 360;
            }
        }
    }

    private void init(){
        state = State.IDLE;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setClipToOutline(true);
            setOutlineProvider(new QueueButtonOutlineProvider());
        }

        this.backgroundPaint = new Paint();
        this.backgroundPaint.setColor(getResources().getColor(R.color.colorPrimary));
        this.backgroundPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        this.backgroundPaintStopped = new Paint();
        this.backgroundPaintStopped.setColor(Color.RED);
        this.backgroundPaintStopped.setFlags(Paint.ANTI_ALIAS_FLAG);

        this.animatedBallPaint = new Paint();
        this.animatedBallPaint.setColor(getResources().getColor(R.color.colorAccent));
        this.animatedBallPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        this.textPaint = new Paint();
        this.textPaint.setColor(Color.WHITE);
        this.textPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        this.textPaint.setTextAlign(Paint.Align.CENTER);
        this.textPaint.setTextSize(50);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Try for a width based on our minimum
        int minw = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();
        int w = resolveSizeAndState(minw, widthMeasureSpec, 1);

        // Whatever the width ends up being, ask for a height that would let the pie
        // get as big as it can
        int h = resolveSizeAndState(MeasureSpec.getSize(w), widthMeasureSpec, 0);

        setMeasuredDimension(w, h);
    }

    public void setLeftPeople(int peopleLeft) {
        this.peopleLeft = peopleLeft;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private class QueueButtonOutlineProvider extends ViewOutlineProvider {
        @Override
        public void getOutline(View view, Outline outline) {
            outline.setOval(view.getPaddingLeft(), view.getPaddingRight(), view.getWidth() - view.getPaddingRight(), view.getHeight() - view.getPaddingBottom());
        }
    }

    public enum State{
        IDLE,
        RUNNING,
        STOPPED
    }

    public void setState(State state){
        if(state == State.STOPPED || state == State.IDLE)
            stopAnimation();
        else
            startAnimation();
        invalidate();
        this.state = state;
    }

    public State getState(){
        return state;
    }

    private void startAnimation(){
        stopAnimation();
        mHandler.post(mTick);
    }

    private void stopAnimation(){
        mHandler.removeCallbacks(mTick);
    }
}
