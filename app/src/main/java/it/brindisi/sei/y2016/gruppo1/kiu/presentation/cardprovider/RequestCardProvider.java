package it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.NumberFormat;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

//import com.github.ornolfr.ratingview.RatingView;

/**
 * Created by Davide on 03/07/2016.
 */
public class RequestCardProvider extends CardProvider<RequestCardProvider> implements View.OnClickListener {
    private User user;
    private Listener listener;

    public QueueRequest getRequest() {
        return request;
    }

    public RequestCardProvider setRequest(QueueRequest request) {
        this.request = request;
        return this;
    }

    public RequestCardProvider setUser(User user) {
        this.user = user;
        return this;
    }

    public RequestCardProvider setListener(Listener listener) {
        this.listener = listener;
        return this;
    }

    private QueueRequest request;

    @Override
    protected void onCreated() {
        super.onCreated();
        setLayout(R.layout.card_queue_info);
    }

    @Override
    public void render(@NonNull View view, @NonNull Card card) {
        if (request != null) {
            DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
            DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
            TextView date = (TextView) view.findViewById(R.id.date);
            date.setText(dateFormat.format(request.getStartTime()));
            TextView time = (TextView) view.findViewById(R.id.time);
            time.setText(timeFormat.format(request.getStartTime()));
            TextView fare = (TextView) view.findViewById(R.id.fare);
            NumberFormat nf = NumberFormat.getCurrencyInstance();
            if (user != null) {
                view.findViewById(R.id.kiuer_info).setVisibility(View.VISIBLE);
                ImageView helperProfilePicture = (ImageView) view.findViewById(R.id.user_picture);
                Picasso.with(getContext()).load(user.getPhotoUrl()).placeholder(R.drawable.user_avatar).transform(new CropCircleTransformation()).into(helperProfilePicture);
                TextView name = (TextView) view.findViewById(R.id.user_name);
                name.setText(user.getName());
                RatingBar rating = (RatingBar) view.findViewById(R.id.ratingBar);
                if(user.isHelper()) {
                    fare.setText(nf.format(user.getHelperSettings().getFare() / 100f));
                    rating.setRating(user.getHelperRating());
                }else {
                    fare.setText(nf.format(request.getHelper().getHelperSettings().getFare() / 100f));
                    rating.setRating(user.getUserRating());
                }
            }else{
                view.findViewById(R.id.no_helper).setVisibility(View.VISIBLE);
                Button selectHelperButton = (Button) view.findViewById(R.id.select_helper_button);
                selectHelperButton.setOnClickListener(this);
                fare.setText(nf.format(request.getMaxFare() / 100f));
            }
        }
    }

    @Override
    public void onClick(View view) {
        if(listener != null){
            listener.onSelectHelper(request);
        }
    }

    public interface Listener {
        void onSelectHelper(QueueRequest request);
    }
}
