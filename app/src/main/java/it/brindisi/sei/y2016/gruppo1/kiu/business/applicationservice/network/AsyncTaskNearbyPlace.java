package it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.network;

import android.os.AsyncTask;

import com.github.kevinsawicki.http.HttpRequest;

import java.util.Locale;

import it.brindisi.sei.y2016.gruppo1.kiu.utility.Constants;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

/**
 * Created by Davide on 08/06/2016.
 */
public class AsyncTaskNearbyPlace extends AsyncTask<String, Void, String>{

    private static final String PLACE_KEY = Constants.GOOGLE_API_KEY;
    private final String PLACE_NEARBY_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key="+PLACE_KEY+"&location=%1$s&rankby=distance&keyword=%2$s";
    private final OnCompleteListener<String> onCompleteListener;

    public AsyncTaskNearbyPlace(OnCompleteListener<String> onCompleteListener) {
        this.onCompleteListener = onCompleteListener;
    }

    @Override
    protected void onPostExecute(String s) {
        this.onCompleteListener.onComplete(true, s);
    }

    @Override
    protected String doInBackground(String... params) {
        String query = params[0].replace(" ", "+");
        String location = params[1];
        HttpRequest request = HttpRequest.get(String.format(Locale.US, PLACE_NEARBY_URL, location, query));
        String json = request.body();
        return json;
    }
}
