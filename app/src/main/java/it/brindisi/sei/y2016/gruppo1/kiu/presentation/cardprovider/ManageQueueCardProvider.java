package it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity.RegistrationActivity;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.view.QueueButton;

/**
 * Created by Davide on 06/07/2016.
 */
public class ManageQueueCardProvider extends CardProvider<ManageQueueCardProvider> implements View.OnClickListener {
    private QueueRequest request;
    private OnQueueListener listener;
    private EditText peopleEditText;
    private QueueButton queueButton;
    private int people = -1;
    private TextView queue_text;

    public ManageQueueCardProvider setRequest(QueueRequest request) {
        this.request = request;
        return this;
    }

    @Override
    protected void onCreated() {
        super.onCreated();
        setLayout(R.layout.card_manage_queue);
    }

    @Override
    public void render(@NonNull View view, @NonNull Card card) {
        queue_text = (TextView) view.findViewById(R.id.queue_text);

        if (request != null) {
            queueButton = (QueueButton) view.findViewById(R.id.queueButton);
            queueButton.setOnClickListener(this);
            peopleEditText = (EditText) view.findViewById(R.id.number_of_people);
            if(people != -1){
                queueButton.setState(QueueButton.State.RUNNING);
                queueButton.setLeftPeople(people);
                peopleEditText.setEnabled(false);
                peopleEditText.setText(String.valueOf(people));

            }
        }


    }

    public ManageQueueCardProvider setListener(OnQueueListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    public void onClick(View v) {
        int peopleLeft = Integer.parseInt(peopleEditText.getText().toString());

        if (queueButton.getState() == QueueButton.State.IDLE) {
            if(peopleLeft <= 0){
                showDialogNoPeople();
            }else {
                queueButton.setState(QueueButton.State.RUNNING);
                queueButton.setLeftPeople(peopleLeft);
                peopleEditText.setEnabled(false);
                if (listener != null)
                    listener.onStartQueue(peopleLeft);
                if(peopleLeft > 2){
                    queue_text.setTextColor(Color.parseColor("#43B854"));
                }else if (peopleLeft >= 1 || peopleLeft <= 2) {
                    queue_text.setTextColor(Color.parseColor("#FF9800"));
                }else if(peopleLeft == 0){
                    queue_text.setTextColor(Color.parseColor("#D50000"));
                }
            }
        } else if (queueButton.getState() == QueueButton.State.RUNNING) {
            peopleEditText.setText("" + (--peopleLeft));

            if(peopleLeft > 2){
                queue_text.setTextColor(Color.parseColor("#43B854"));
            }else if (peopleLeft >= 1 || people <= 2) {
                queue_text.setTextColor(Color.parseColor("#FF9800"));
            }

            queueButton.setLeftPeople(peopleLeft);
            if (peopleLeft == 0) {
                queueButton.setState(QueueButton.State.STOPPED);
                queue_text.setTextColor(Color.parseColor("#D50000"));
            }
            if (listener != null)
                listener.onSubctratPerson(peopleLeft);
        }
    }

    private void showDialogNoPeople() {
        final AlertDialog.Builder miaAlert = new AlertDialog.Builder(this.getContext());
        miaAlert.setMessage(R.string.zero_people);
        miaAlert.setTitle(R.string.attenzione);
        miaAlert.setCancelable(false);
        miaAlert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                queueButton.setState(QueueButton.State.STOPPED);
                listener.onSubctratPerson(0);
            }
        });
        miaAlert.setNegativeButton(android.R.string.no, null);

        AlertDialog alert = miaAlert.create();
        alert.show();
    }

    public interface OnQueueListener {
        void onStartQueue(int people);

        void onSubctratPerson(int people);
    }

    public void activate(int people){
        this.people = people;
    }
}
