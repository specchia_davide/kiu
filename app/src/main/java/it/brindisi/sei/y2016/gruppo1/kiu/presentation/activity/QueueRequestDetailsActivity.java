package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.view.MaterialListView;
import com.google.gson.Gson;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceQueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.exception.TooLateException;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Place;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequestStatus;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider.PlaceCardProvider;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider.RatingCardProvider;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider.RequestCardProvider;

/**
 * Created by Anonym on 01/07/2016.
 */
public class QueueRequestDetailsActivity extends AppCompatActivity implements RequestCardProvider.Listener, RatingCardProvider.Listener {

    private static final int SELECT_HELPER_CODE = 1;
    QueueRequest request;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.queue_request_details_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Gson gson = new Gson();
        request = gson.fromJson(getIntent().getExtras().getString("request"), QueueRequest.class);

        MaterialListView mListView = (MaterialListView) findViewById(R.id.material_listview);

        Card placeCard = new Card.Builder(this)
                .setTag("PLACE_CARD")
                .withProvider(new PlaceCardProvider())
                .setPlace(request.getPlace())
                .endConfig()
                .build();
        mListView.getAdapter().add(placeCard);

        Card requestCard = new Card.Builder(this)
                .setTag("REQUEST_CARD")
                .withProvider(new RequestCardProvider())
                .setRequest(request)
                .setUser(request.getHelper())
                .setListener(this)
                .endConfig()
                .build();
        mListView.getAdapter().add(requestCard);

        Card feedbackCard = new Card.Builder(this)
                .setTag("FEEDBACK_CARD")
                .withProvider(new RatingCardProvider())
                .setKiuerRating(false)
                .setUserRated(request.getHelper())
                .setListener(this)
                .endConfig()
                .build();

        if(request.getStatus() == QueueRequestStatus.WAITING_FOR_CLOSE){
            mListView.getAdapter().addAtStart(feedbackCard);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.delete_cart:
                ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
                try {
                    asqr.removeRequest(request);
                } catch (TooLateException e) {
                    e.printStackTrace();
                }
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSelectHelper(QueueRequest request) {
        Intent intent = new Intent(this, SelectHelperActivity.class);
        Gson gson = new Gson();
        intent.putExtra("request", gson.toJson(request));
        startActivityForResult(intent, SELECT_HELPER_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(requestCode == SELECT_HELPER_CODE){
                User helper = (User) data.getSerializableExtra("helper");
                request.setHelper(helper);
                request.setStatus(QueueRequestStatus.WAITING_FOR_QUEUE);
                ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
                try {
                    asqr.editRequest(request);
                } catch (TooLateException e) {
                    Crouton.makeText(this, getString(R.string.too_late_to_edit), Style.ALERT).show();
                }
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tresh_menu, menu);
        return true;
    }


    @Override
    public void onFeedback() {
        ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
        asqr.closeRequestForKiuer(request);
        request.setStatus(QueueRequestStatus.CLOSED);
    }
}
