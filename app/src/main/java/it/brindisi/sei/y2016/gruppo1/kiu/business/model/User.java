package it.brindisi.sei.y2016.gruppo1.kiu.business.model;

import android.net.Uri;

import java.io.Serializable;

/**
 * Created by Davide on 21/06/2016.
 */
public class User implements Serializable{
    private String name;
    private String email;
    private String id;
    private int userRating;
    private int helperRating;
    private String photoUrl;
    private HelperSettings helperSettings;

    public HelperSettings getHelperSettings() {
        return helperSettings;
    }

    public void setHelperSettings(HelperSettings helperSettings) {
        this.helperSettings = helperSettings;
    }

    public int getUserRating() {
        return userRating;
    }

    public void setUserRating(int userRating) {
        this.userRating = userRating;
    }

    public int getHelperRating() {
        return helperRating;
    }

    public void setHelperRating(int helperRating) {
        this.helperRating = helperRating;
    }

    public boolean isHelper(){
        return helperSettings != null;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        String tmp = "";
        String[] tokens=name.split(" ");
        for(int i = 1; i < tokens.length - 1; i++){
            tmp += tokens[i] + " ";
        }
        tmp += tokens[tokens.length - 1];
        return tmp;
    }

    public String getFirstName(){
        return name.split(" ")[0];
    }
}
