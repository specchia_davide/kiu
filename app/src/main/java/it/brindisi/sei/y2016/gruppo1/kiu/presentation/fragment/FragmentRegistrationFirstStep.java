package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import it.brindisi.sei.y2016.gruppo1.kiu.R;

/**
 * Created by Davide on 01/06/2016.
 */
public class FragmentRegistrationFirstStep extends Fragment implements View.OnClickListener {

    private Listener listener;
    private ImageView profilePictureImageView;
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;
    private Bitmap profilePicture;

    @Override
    public void onClick(View v) {
        ConnectivityManager connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            switch (v.getId()) {
                case R.id.profilePicture:
                    listener.onPickPicture();
                    break;
                case R.id.creaAccount:
                    if (checkInformations())
                        listener.onNext(firstName.getText().toString(),
                                lastName.getText().toString(),
                                email.getText().toString(),
                                password.getText().toString(),
                                profilePicture);
                    break;


            }
        }
        else{
            Crouton.makeText(getActivity(), "Connessione non Trovata", Style.ALERT, R.id.search_view_item).show();
        }
    }

    public boolean checkInformations() {
        boolean isValid = true;
        if (TextUtils.isEmpty(firstName.getText().toString().trim())) {
            isValid = false;
            firstName.setError(getString(R.string.firstname_empty_error));
        }

        if (TextUtils.isEmpty(lastName.getText().toString().trim())) {
            isValid = false;
            lastName.setError(getString(R.string.lastname_empty_error));
        }

        if (TextUtils.isEmpty(email.getText().toString().trim())) {
            isValid = false;
            email.setError(getString(R.string.email_empty_error));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            isValid = false;
            email.setError(getString(R.string.email_invalid_error));
        }

        if (TextUtils.isEmpty(password.getText().toString())) {
            isValid = false;
            password.setError(getString(R.string.password_empty_error));
        } else if (password.getText().toString().length() < 6) {
            isValid = false;
            password.setError(getString(R.string.password_too_short_error));
        }
        return isValid;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setProfilePicture(Bitmap profilePicture) {
        this.profilePicture = profilePicture;
        this.profilePictureImageView.setImageBitmap(profilePicture);
    }

    public interface Listener {
        void onNext(String firstName, String lastName, String email, String password, Bitmap picture);

        void onPickPicture();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("firstName", getFirstName());
        outState.putString("lastName", getLastName());
        outState.putString("email", getEmail());
        outState.putString("password", getPassword());
        //outState.putSerializable("profilePicture", getProfilePicture());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_registation, container, false);

        profilePictureImageView = (ImageView) v.findViewById(R.id.profilePicture);
        profilePictureImageView.setOnClickListener(this);

        firstName = (EditText) v.findViewById(R.id.nome);
        lastName = (EditText) v.findViewById(R.id.cognome);
        email = (EditText) v.findViewById(R.id.email);
        password = (EditText) v.findViewById(R.id.password);

        Button next = (Button) v.findViewById(R.id.creaAccount);
        next.setOnClickListener(this);


        if (savedInstanceState != null) {

            firstName.setText(savedInstanceState.getString("firstName"));
            lastName.setText(savedInstanceState.getString("lastName"));
            email.setText(savedInstanceState.getString("email"));
            password.setText(savedInstanceState.getString("password"));

        }

        return v;
    }



    public String getFirstName(){return firstName.getText().toString();}

    public String getLastName(){
        return lastName.getText().toString();
    }

    public String getEmail() {
        return email.getText().toString();
    }

    public String getPassword(){
        return password.getText().toString();
    }

    public Bitmap getProfilePicture(){
        return profilePicture;
    }
}
