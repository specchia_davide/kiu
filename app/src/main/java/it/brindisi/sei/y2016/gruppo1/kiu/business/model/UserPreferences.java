package it.brindisi.sei.y2016.gruppo1.kiu.business.model;

/**
 * Created by Davide on 04/07/2016.
 */
public class UserPreferences {
    private boolean kiuerHome = true; //se true apre la home dei kiuer

    public boolean isKiuerHome() {
        return kiuerHome;
    }

    public void setKiuerHome(boolean kiuerHome) {
        this.kiuerHome = kiuerHome;
    }
}
