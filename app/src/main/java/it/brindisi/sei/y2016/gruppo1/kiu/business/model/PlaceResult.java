package it.brindisi.sei.y2016.gruppo1.kiu.business.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Davide on 08/06/2016.
 */
public class PlaceResult implements Serializable{
    @SerializedName("next_page_token")
    private String nextPageToken;

    private List<Place> results;

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public List<Place> getResults() {
        return results;
    }

    public void setResults(List<Place> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    public void mergeResults(PlaceResult r) {
        for(Place newPlace : r.getResults()){
            boolean isPresent = false;
            for(Place oldPlace : results){
                if(newPlace.getPlaceId().equals(oldPlace.getPlaceId())) {
                    isPresent = true;
                    break;
                }
            }
            if(!isPresent){
                results.add(newPlace);
            }
        }
    }
}
