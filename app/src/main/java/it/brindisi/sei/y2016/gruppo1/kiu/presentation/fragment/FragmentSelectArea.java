package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.HelperSettings;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Location;

/**
 * Created by Davide on 30/06/2016.
 */
public class FragmentSelectArea extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, SeekBar.OnSeekBarChangeListener, View.OnClickListener {
    private static final int RADIUS_SCALE_FACTOR = 500;

    private GoogleMap googleMap;
    private LatLng center = new LatLng(0 , 0);
    private int radius;
    private Listener listener;
    private HelperSettings hs;
    private SeekBar radiusBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_select_area, container, false);
        radiusBar = (SeekBar) v.findViewById(R.id.radiusBar);
        radiusBar.setOnSeekBarChangeListener(this);
        radius = radiusBar.getProgress();
        Button next = (Button) v.findViewById(R.id.next);
        next.setOnClickListener(this);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment smf = new SupportMapFragment();
        getChildFragmentManager().beginTransaction().replace(R.id.map, smf).commit();
        smf.getMapAsync(this);
    }

    private void drawSelectedArea(LatLng latLng, int radius){
        this.googleMap.clear();
        MarkerOptions marker = new MarkerOptions();
        marker.position(latLng);
        this.googleMap.addMarker(marker);
        CircleOptions circle = new CircleOptions();
        circle.center(latLng);
        circle.radius(radius * RADIUS_SCALE_FACTOR);
        int strokeColor = getResources().getColor(R.color.colorAccent);
        circle.strokeColor(strokeColor);
        circle.strokeWidth(5);
        int fillColor = strokeColor - 0xE6000000; //Abbasso il canale alfa al 10% circa
        circle.fillColor(fillColor);
        this.googleMap.addCircle(circle);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setOnMapClickListener(this);
        if(hs != null) {
            center = new LatLng(hs.getCenter().getLat(), hs.getCenter().getLng());
            radiusBar.setProgress(hs.getRadius()/RADIUS_SCALE_FACTOR);
            onMapClick(center);
            CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(center, 10);
            this.googleMap.moveCamera(cu);
        }
    }

    @Override
    public void onMapClick(LatLng latLng){
        center = latLng;
        drawSelectedArea(latLng, radius);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        radius = progress + 1; //Evito che il raggio sia 0
        drawSelectedArea(center, radius);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next:
                if(checkLocation()) {
                    Location location = new Location();
                    location.setLat(center.latitude);
                    location.setLng(center.longitude);
                    this.listener.onNext(location, radius * RADIUS_SCALE_FACTOR);
                }else{
                    Crouton.makeText(getActivity(), getString(R.string.select_location), Style.ALERT).show();
                }
        }
    }

    private boolean checkLocation() {
        return center != null;
    }

    public void setDefault(HelperSettings helperSettings) {
        hs = helperSettings;
    }

    public interface Listener {
        void onNext(Location location, int radius);
    }

    public void setListener(Listener listener){
        this.listener = listener;
    }
}
