package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceHelper;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.HelperSettings;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Location;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.ImagePicker;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by degiu on 05/07/2016.
 */
public class FragmentSettingsKiuer extends Fragment implements View.OnClickListener {
    private static final int IMAGE_PICK = 17;
    private Bitmap bitmap;
    private ImageView profilePicture;
    private ApplicationServiceUser user = new ApplicationServiceUser();
    private EditText email;
    private EditText nome;
    private EditText cognome;
    private View helper_settings;
    private Listener listener;
    private boolean isHelper;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        User utente = user.getLoggedUser();

        utente.getEmail();
        utente.getName();
        utente.getLastName();
        View v = inflater.inflate(R.layout.fragment_settings_kiuer, container, false);

        profilePicture = (ImageView) v.findViewById(R.id.profilePicture);

        Picasso.with(getActivity())
                .load(utente.getPhotoUrl())
                .placeholder(R.drawable.user_avatar)
                .transform(new CropCircleTransformation())
                .into(profilePicture);

        email = (EditText) v.findViewById(R.id.email);
        email.setText(utente.getEmail());

        nome = (EditText) v.findViewById(R.id.nome);
        nome.setText(utente.getFirstName());

        cognome = (EditText) v.findViewById(R.id.cognome);
        cognome.setText(utente.getLastName());

        Button confirm = (Button) v.findViewById(R.id.confirm);
        Button areaSett = (Button) v.findViewById(R.id.area_settings);
        Button disp = (Button) v.findViewById(R.id.disponibilita);

        confirm.setOnClickListener(this);
        areaSett.setOnClickListener(this);
        disp.setOnClickListener(this);

        profilePicture.setOnClickListener(this);

        helper_settings=v.findViewById(R.id.helper_container);
        if(isHelper)
            isHelper();
        return v;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.confirm:
                listener.onConfirm(email.getText().toString(), nome.getText().toString(), cognome.getText().toString(), bitmap);
                break;
            case R.id.profilePicture:
                Intent intent = ImagePicker.getPickImageIntent(getActivity());
                startActivityForResult(intent,IMAGE_PICK);
                break;
            case R.id.area_settings:
                listener.onAreaSettings();
                break;
            case R.id.disponibilita:
                listener.onDispSettings();
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==IMAGE_PICK){
            bitmap = ImagePicker.getImageFromResult(getActivity(), resultCode, data);
            if (bitmap != null) {
                Bitmap b2 = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                CropCircleTransformation cropper = new CropCircleTransformation();
                profilePicture.setImageBitmap(cropper.transform(b2));
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setListener(Listener l){
        this.listener = l;
    }

    public void isHelper() {
        isHelper = true;
        helper_settings.setVisibility(View.VISIBLE);
    }

    public interface Listener{
        void onAreaSettings();
        void onDispSettings();
        void onConfirm(String email, String name, String lastName, Bitmap picture);
    }

}
