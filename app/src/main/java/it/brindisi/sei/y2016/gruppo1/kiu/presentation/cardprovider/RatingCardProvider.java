package it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.squareup.picasso.Picasso;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by Anonym on 07/07/2016.
 */
public class RatingCardProvider extends CardProvider<RatingCardProvider> implements View.OnClickListener, OnCompleteListener<Void> {
    private User userRated;
    private boolean kiuerFeedback = false;
    private RatingBar rating;
    private View base, progress, thanks;
    private Listener listener;

    @Override
    protected void onCreated() {
        super.onCreated();
        setLayout(R.layout.card_rating_user);
    }

    @Override
    public void render(@NonNull View v, @NonNull Card card) {

        TextView nome = (TextView)v.findViewById(R.id.name);
        rating = (RatingBar) v.findViewById(R.id.ratingBar);
        ImageView img = (ImageView) v.findViewById(R.id.profilePicture);

        nome.setText(userRated.getName());

        Picasso.with(getContext()).load(userRated.getPhotoUrl()).placeholder(R.drawable.user_avatar).transform(new CropCircleTransformation()).into(img);

        Button confirmButton = (Button) v.findViewById(R.id.feedback_button);
        confirmButton.setOnClickListener(this);

        base =v.findViewById(R.id.rating_container);
        progress = v.findViewById(R.id.progress_container);
        thanks = v.findViewById(R.id.thanks_container);

    }

    public RatingCardProvider setUserRated(User userRated) {
        this.userRated = userRated;
        return this;
    }

    public RatingCardProvider setKiuerRating(boolean isKiuerRating) {
        this.kiuerFeedback = isKiuerRating;
        return this;
    }

    public RatingCardProvider setListener(Listener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    public void onClick(View view) {
        base.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);
        ApplicationServiceUser asu = new ApplicationServiceUser();
        if(kiuerFeedback){
            asu.leaveUserFeedback(userRated, rating.getProgress(), this);
        }else{
            asu.leaveHelperFeedback(userRated, rating.getProgress(), this);
        }
    }

    @Override
    public void onComplete(boolean success, Void result) {
        progress.setVisibility(View.GONE);
        thanks.setVisibility(View.VISIBLE);
        this.listener.onFeedback();
    }

    public interface Listener {
        void onFeedback();
    }
}
