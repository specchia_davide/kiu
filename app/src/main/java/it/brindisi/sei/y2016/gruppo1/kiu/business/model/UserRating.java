package it.brindisi.sei.y2016.gruppo1.kiu.business.model;

/**
 * Created by Davide on 09/07/2016.
 */
public class UserRating {
    private int numberOfFeedback;
    private float rating;

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getNumberOfFeedback() {
        return numberOfFeedback;
    }

    public void setNumberOfFeedback(int numberOfFeedback) {
        this.numberOfFeedback = numberOfFeedback;
    }
}
