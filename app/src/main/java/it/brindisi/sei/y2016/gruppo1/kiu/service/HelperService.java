package it.brindisi.sei.y2016.gruppo1.kiu.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.util.Date;
import java.util.Map;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceQueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequestStatus;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity.HelperHomeActivity;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity.HelperRequestDetailsActivity;

/**
 * Created by Davide on 05/07/2016.
 */
public class HelperService extends Service implements ApplicationServiceQueueRequest.RequestChangedListener {



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationServiceUser asu = new ApplicationServiceUser();
        User loggedUser = asu.getLoggedUser();
        if(loggedUser != null){
            ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
            asqr.setHelperRequestChangedListener(this);
        }else{
            stopSelf();
        }
    }

    @Override
    public void onRequestRead(Map<String, QueueRequest> requests) {

    }

    @Override
    public void onRequestChanged(String key, QueueRequest value) {
        if(value.getStatus() == QueueRequestStatus.WAITING_FOR_QUEUE)
            sendNotification(getString(R.string.request_changed_notification_title), getString(R.string.new_request_changed_desc), value);
    }

    @Override
    public void onRequestAdded(String key, QueueRequest value) {
        sendNotification(getString(R.string.new_request_notification_title), getString(R.string.new_request_notification_desc), value);
    }

    @Override
    public void onRequestRemoved(String key, QueueRequest value) {
        //sendNotification(getString(R.string.new_request_notification_title), getString(R.string.new_request_notification_desc), value);
    }

    private void sendNotification(String title, String description, QueueRequest request){
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentTitle(title)
                .setContentText(description)
                .setSound(notificationSound);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(this.getString(R.string.request_details));
        inboxStyle.addLine(request.getUser().getName());
        inboxStyle.addLine(request.getPlace().getName());
        inboxStyle.addLine(request.getPlace().getFormattedAddress());
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
        inboxStyle.addLine(df.format(request.getStartTime()));

        notificationBuilder.setStyle(inboxStyle);

        Intent intent = new Intent(this, HelperRequestDetailsActivity.class);

        Gson gson = new Gson();
        intent.putExtra("request", gson.toJson(request));

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
        taskStackBuilder.addParentStack(HelperHomeActivity.class);
        taskStackBuilder.addNextIntent(intent);

        PendingIntent resultPendingIntent =
                taskStackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        notificationBuilder.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(1, notificationBuilder.build());
    }
}
