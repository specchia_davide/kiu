package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.Date;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceQueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Place;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentConfirmPlace;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentSelectHelper;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.Constants;

public class ResultDetailsActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, FragmentConfirmPlace.OnCreateRequestListener, FragmentSelectHelper.Listener {

    private static final String PLACE_PHOTO_URL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key=" + Constants.GOOGLE_API_KEY + "&photoreference=";
    private Place place;
    FloatingActionButton fab;
    private FragmentConfirmPlace fragmentConfirmPlace;
    private FragmentSelectHelper fragmentSelectHelper;
    private SupportMapFragment supportMapFragment;
    private QueueRequest queueRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_details);

        this.place = (Place) getIntent().getSerializableExtra("place");

        TextView placeName = (TextView) findViewById(R.id.place_name);
        placeName.setText(place.getName());
        TextView placeAddress = (TextView) findViewById(R.id.place_address);
        placeAddress.setText(place.getFormattedAddress());
        ImageView placeImage = (ImageView) findViewById(R.id.placeImage);
        if(place.getPhotoReference() != null) {
            Picasso.with(this).load(PLACE_PHOTO_URL + place.getPhotoReference()).into(placeImage);
        }else{
            placeImage.setVisibility(View.GONE);
        }
        supportMapFragment = new SupportMapFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contain, supportMapFragment).commit();
            supportMapFragment.getMapAsync(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        fragmentConfirmPlace = new FragmentConfirmPlace();
        fragmentConfirmPlace.setListener(this);

        fragmentSelectHelper = new FragmentSelectHelper();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng center = new LatLng(place.getLocation().getLat(), place.getLocation().getLng());

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(center);
        googleMap.addMarker(markerOptions);

        CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(center, 16);
        googleMap.moveCamera(camera);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contain, fragmentConfirmPlace).addToBackStack(null).commit();
                fab.hide();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(supportMapFragment.isAdded())
            fab.show();
    }

    @Override
    public void onCreateRequest(Date date, int fare, String note) {
        queueRequest = new QueueRequest();
        queueRequest.setStartTime(date);
        queueRequest.setPlace(place);
        queueRequest.setMaxFare(fare);
        queueRequest.setNote(note);
        ApplicationServiceUser asu = new ApplicationServiceUser();
        queueRequest.setUser(asu.getLoggedUser());

        fragmentSelectHelper = new FragmentSelectHelper();
        fragmentSelectHelper.setQueueRequest(queueRequest);
        fragmentSelectHelper.setListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.contain, fragmentSelectHelper).addToBackStack(null).commit();

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onHelperSelected(User h) {
        queueRequest.setHelper(h);
        ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
        asqr.addNewRequest(queueRequest);

        Intent intent = new Intent(this, KiuerHomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}

