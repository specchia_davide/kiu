package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;

import org.w3c.dom.Text;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;

/**
 * Created by Matteo on 01/06/2016.
 */
public class FragmentForgottPass extends Fragment implements View.OnClickListener{

    private EditText email;

    @Override
    public void onClick(View v) {

        ApplicationServiceUser aso = new ApplicationServiceUser();
        aso.resetPassword(email.getText().toString(), null);

        AlertDialog.Builder miaAlert = new AlertDialog.Builder(getActivity());
        miaAlert.setMessage(getString(R.string.email_conferma));

        miaAlert.setCancelable(false);
        miaAlert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                getActivity().getSupportFragmentManager().popBackStackImmediate();

            }
        });

        AlertDialog alert = miaAlert.create();
        alert.show();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_forgotten_pass, container, false);

        email = (EditText) v.findViewById(R.id.email);

        Button button = (Button) v.findViewById(R.id.button_pass_forgotten);
        button.setOnClickListener(this);

        return v;
    }
}
