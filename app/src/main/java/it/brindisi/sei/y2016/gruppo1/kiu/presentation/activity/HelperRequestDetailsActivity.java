package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.view.MaterialListView;
import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceQueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.exception.TooLateException;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequestStatus;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider.ManageQueueCardProvider;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider.PlaceCardProvider;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider.RatingCardProvider;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider.RequestCardProvider;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.view.QueueButton;

/**
 * Created by Matteo on 05/07/2016.
 */
public class HelperRequestDetailsActivity extends AppCompatActivity implements ManageQueueCardProvider.OnQueueListener, View.OnClickListener, RatingCardProvider.Listener {

    FloatingActionButton fabh;
    QueueRequest request;
    List<Card> cardList;
    private Card manageCard;
    MaterialListView listView;
    private Card ratingCard;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.helper_request_details_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        Gson gson = new Gson();
        request = gson.fromJson(getIntent().getExtras().getString("request"), QueueRequest.class);

        cardList = new LinkedList<>();
        listView = (MaterialListView) findViewById(R.id.material_listview);


        manageCard = new Card.Builder(this)
                .setTag("MANAGE_CARD")
                .withProvider(new ManageQueueCardProvider())
                .setRequest(request)
                .setListener(this)
                .endConfig()
                .build();

        ratingCard = new Card.Builder(this)
                .setTag("RATING_CARD")
                .withProvider(new RatingCardProvider())
                .setUserRated(request.getUser())
                .setKiuerRating(true)
                .setListener(this)
                .endConfig()
                .build();
        //cardList.add(ratingCard);

        Card placeCard = new Card.Builder(this)
                .setTag("PLACE_CARD")
                .withProvider(new PlaceCardProvider())
                .setPlace(request.getPlace())
                .endConfig()
                .build();
        cardList.add(placeCard);

        Card queueCard = new Card.Builder(this)
                .setTag("REQUEST_CARD")
                .withProvider(new RequestCardProvider())
                .setRequest(request)
                .setUser(request.getUser())
                .endConfig()
                .build();
        cardList.add(queueCard);

        listView.getAdapter().addAll(cardList);

        fabh = (FloatingActionButton) findViewById(R.id.fabh);
        fabh.setOnClickListener(this);
        if(request.getStatus()!=QueueRequestStatus.WAITING_FOR_QUEUE){
            fabh.hide();
        }
        if(request.getStatus() == QueueRequestStatus.WAITING_FOR_CLOSE){
            listView.getAdapter().addAtStart(ratingCard);
        }else  if(request.getStatus()==QueueRequestStatus.ACTIVE){
            ((ManageQueueCardProvider)manageCard.getProvider()).activate(request.getQueue());
            listView.getAdapter().addAtStart(manageCard);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;

            case R.id.delete_cart:
                ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
                try {
                    asqr.removeHelper(request);
                } catch (TooLateException e) {
                    e.printStackTrace();
                }
                finish();
                break;
        }
        return true;

    }

    @Override
    public void onStartQueue(int people) {
        if(people > 0) {
            request.setStatus(QueueRequestStatus.ACTIVE);
            request.setQueue(people);
            ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
            asqr.startRequest(request);
        }
    }

    @Override
    public void onSubctratPerson(int people) {
        ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
        if(people==0){
            listView.getAdapter().addAtStart(ratingCard);
        }
        asqr.subctratPersonRequest(request);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabh:
                listView.getAdapter().addAtStart(manageCard);
                fabh.hide();
                break;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tresh_menu, menu);
        return true;
    }


    @Override
    public void onFeedback() {
        ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
        asqr.closeRequestForHelper(request);
    }
}
