package it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Location;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.PlaceResult;

/**
 * Created by Davide on 08/06/2016.
 */
public class SearchResultsAdapter extends BaseAdapter{
    PlaceResult placeResult;
    private Context context;
    private Location currentLocation;

    public SearchResultsAdapter(PlaceResult placeResult, Location currentLocation, Context context) {
        this.placeResult = placeResult;
        this.currentLocation = currentLocation;
        this.context = context;
    }

    @Override
    public int getCount() {
        return placeResult.getResults().size();
    }

    @Override
    public Object getItem(int position) {
        return placeResult.getResults().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null){
            v = LayoutInflater.from(context).inflate(R.layout.place_list_item, null);
        }
        TextView name = (TextView) v.findViewById(R.id.name);
        name.setText(placeResult.getResults().get(position).getName());
        TextView address = (TextView) v.findViewById(R.id.address);
        address.setText(placeResult.getResults().get(position).getFormattedAddress());
        TextView distance = (TextView) v.findViewById(R.id.distance);
        if(currentLocation != null) {
            int distanceKm = (int) currentLocation.distanceTo(placeResult.getResults().get(position).getLocation()) / 1000;
            distance.setText(context.getString(R.string.distance_km, distanceKm));
        }else{
            distance.setVisibility(View.GONE);
        }
        return v;
    }
}
