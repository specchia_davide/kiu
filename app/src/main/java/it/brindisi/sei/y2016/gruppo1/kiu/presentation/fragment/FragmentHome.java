package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.provider.ListCardProvider;
import com.dexafree.materialList.view.MaterialListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceQueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter.AttivitaNeiDintorniAdapter;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter.QueueRequestAdapter;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter.PreferitiAdapter;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider.HintCardProvider;


public class FragmentHome extends Fragment {

    Listener listener;
    QueueRequestAdapter code_in_corso;
    boolean code_in_corso_added = false;
    private Card card_hint;
    private ArrayList<Card> cards;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        cards = new ArrayList<>();
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        final MaterialListView mListView = (MaterialListView) v.findViewById(R.id.material_listview);

        code_in_corso = new QueueRequestAdapter(getActivity());
        final Card card_code_in_corso = new Card.Builder(getActivity())
                .setTag("LIST_CARD")
                .withProvider(new ListCardProvider())
                .setLayout(R.layout.material_list_card_layout)
                .setTitle(getString(R.string.code_in_corso))
                .setAdapter(code_in_corso)
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        listener.onQueueRequestSelected((QueueRequest) code_in_corso.getItem(position));
                    }
                })
                .endConfig()
                .build();

        ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
        asqr.setUserRequestChangedListener(new ApplicationServiceQueueRequest.RequestChangedListener() {
            @Override
            public void onRequestRead(Map<String, QueueRequest> requests) {
                if (requests.size() > 0) {
                    for (String key : requests.keySet()) {
                        code_in_corso.putItem(key, requests.get(key));
                    }
                    ((ListCardProvider) card_code_in_corso.getProvider()).setAdapter(code_in_corso);
                    if (!code_in_corso_added) {
                        cards.add(0, card_code_in_corso);
                        code_in_corso_added = true;
                        cards.remove(card_hint);
                        mListView.getAdapter().clearAll();
                        mListView.getAdapter().addAll(cards);
                    }
                }
            }

            @Override
            public void onRequestChanged(String key, QueueRequest value) {
                code_in_corso.putItem(key, value);
                ((ListCardProvider) card_code_in_corso.getProvider()).setAdapter(code_in_corso);
            }

            @Override
            public void onRequestAdded(String key, QueueRequest value) {
                code_in_corso.putItem(key, value);
                ((ListCardProvider) card_code_in_corso.getProvider()).setAdapter(code_in_corso);
                if (!code_in_corso_added) {
                    cards.add(0, card_code_in_corso);
                    code_in_corso_added = true;
                    cards.remove(card_hint);
                    mListView.getAdapter().clearAll();
                    mListView.getAdapter().addAll(cards);
                }
            }

            @Override
            public void onRequestRemoved(String key, QueueRequest value) {
                code_in_corso.removeItem(key);
                ((ListCardProvider) card_code_in_corso.getProvider()).setAdapter(code_in_corso);
                if (code_in_corso.isEmpty()) {
                    cards.remove(card_code_in_corso);
                    if (cards.size() == 0) {
                        cards.add(card_hint);
                    }

                    mListView.getAdapter().clearAll();
                    mListView.getAdapter().addAll(cards);
                }
            }
        });

        /*PreferitiAdapter preferiti = new PreferitiAdapter(getActivity());
        Card card_preferiti= new Card.Builder(getActivity())
                .setTag("LIST_CARD")
                .withProvider(new ListCardProvider())
                .setLayout(R.layout.material_list_card_layout)
                .setTitle(getString(R.string.favorites))
                .setAdapter(preferiti)
                .endConfig()
                .build();
        if(preferiti.getCount()!=0)
            //cards.add(card_preferiti);*/
        card_hint = new Card.Builder(getActivity())
                .setTag("HINT_CARD")
                .withProvider(new HintCardProvider())
                .endConfig()
                .build();
        if (cards.size() == 0) {

            cards.add(card_hint);
        }

        mListView.getAdapter().addAll(cards);

        // Inflate the layout for this fragment
        return v;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        void onQueueRequestSelected(QueueRequest queueRequest);
    }
}