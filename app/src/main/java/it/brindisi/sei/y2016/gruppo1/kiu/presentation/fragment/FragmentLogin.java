package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import it.brindisi.sei.y2016.gruppo1.kiu.R;

/**
 * Created by Matteo on 27/05/2016.
 */
public class FragmentLogin extends Fragment implements View.OnClickListener {
    private LoginListener listener;

    private EditText email, password;
    private String lastLoginEmail;

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.creaAccount:
                listener.onRegistration();
                break;

            case R.id.accedi:
                listener.onLogin(email.getText().toString(), password.getText().toString());
                break;

            case R.id.passDimenticata:
                listener.onForgottenPassword(email.getText().toString());
                break;

        }
    }

    public void setLastLoginEmail(String lastLoginEmail) {
        this.lastLoginEmail = lastLoginEmail;
    }

    public interface LoginListener{
        void onLogin(String email, String password);
        void onForgottenPassword (String email);
        void onRegistration();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        Button registerButton = (Button) v.findViewById(R.id.creaAccount);
        registerButton.setOnClickListener(this);

        Button loginButton = (Button) v.findViewById(R.id.accedi);
        loginButton.setOnClickListener(this);

        email = (EditText) v.findViewById(R.id.email);
        if(lastLoginEmail != null){
            email.setText(lastLoginEmail);
        }

        password = (EditText) v.findViewById(R.id.password);

        TextView passworddimenticata = (TextView) v.findViewById(R.id.passDimenticata);
        passworddimenticata.setOnClickListener(this);

        return v;
    }
    public void setListener(LoginListener listener){
        this.listener = listener;
    }
}
