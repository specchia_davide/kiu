package it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter;

import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Davide on 22/06/2016.
 */
public abstract class DatabaseBaseAdapter<T> extends BaseAdapter {
    private Map<String, T> entries = new HashMap<>();
    private List<String> keys = new ArrayList<>();
    public void putItem(String key, T value){
        entries.put(key, value);
        if(!keys.contains(key)){
            keys.add(key);
        }
        this.notifyDataSetChanged();
    }

    public void removeItem(String key){
        entries.remove(key);
        if(keys.contains(key)){
            keys.remove(key);
        }
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return entries.size();
    }

    @Override
    public Object getItem(int position) {
        return entries.get(keys.get(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
