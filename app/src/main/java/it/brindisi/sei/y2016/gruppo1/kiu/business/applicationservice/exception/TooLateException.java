package it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.exception;

/**
 * Created by Davide on 04/07/2016.
 */
public class TooLateException extends Exception {
    public TooLateException(){
        super("La richiesta può essere modificata entro un massimo di 2 ore dall'inizio");
    }
}
