package it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice;

import android.util.Log;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.exception.TooLateException;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequestStatus;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.UserRating;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

/**
 * Created by danieles on 21/06/16.
 */
public class ApplicationServiceQueueRequest {
    private static final float MAX_HOUR_EDIT = 1;
    DatabaseReference database;
    User user;

    public ApplicationServiceQueueRequest(){
        database = FirebaseDatabase.getInstance().getReference();
        ApplicationServiceUser asu = new ApplicationServiceUser();
        user = asu.getLoggedUser();
    }

    /**
     * Aggiunge una nuova richiesta nell'elenco delle richieste e nell'elenco delle richieste del singolo utente
     * @param request Richiesta da inserire
     */
    public void addNewRequest(final QueueRequest request){
        //Prima di aggiornare recupero il rating dell'utente
        ApplicationServiceUser asu = new ApplicationServiceUser();
        asu.getUserRating(request.getUser(), new OnCompleteListener<UserRating>() {
            @Override
            public void onComplete(boolean success, UserRating result) {
                if(success){
                    request.getUser().setUserRating(Math.round(result.getRating()));
                }
                Calendar calendar = Calendar.getInstance();
                request.setLastEdit(calendar.getTime());

                String key = database.child("queue_requests").push().getKey();
                request.setId(key);
                database.child("queue_requests").child(key).setValue(request);
                updateRequest(request);
            }
        });
    }

    public void setUserRequestChangedListener(final RequestChangedListener listener){
        database.child("users_queue_requests").child(user.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, QueueRequest> requests = new HashMap<>();
                for(DataSnapshot request : dataSnapshot.getChildren()){
                    requests.put(request.getKey(), request.getValue(QueueRequest.class));
                }
                listener.onRequestRead(requests);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        database.child("users_queue_requests").child(user.getId()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                listener.onRequestAdded(dataSnapshot.getKey(), dataSnapshot.getValue(QueueRequest.class));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                listener.onRequestChanged(dataSnapshot.getKey(), dataSnapshot.getValue(QueueRequest.class));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                listener.onRequestRemoved(dataSnapshot.getKey(), dataSnapshot.getValue(QueueRequest.class));
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    public void setHelperRequestChangedListener(final RequestChangedListener listener){
        database.child("helpers_queue_requests").child(user.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, QueueRequest> requests = new HashMap<>();
                for(DataSnapshot request : dataSnapshot.getChildren()){
                    requests.put(request.getKey(), request.getValue(QueueRequest.class));
                }
                listener.onRequestRead(requests);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        database.child("helpers_queue_requests").child(user.getId()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                listener.onRequestAdded(dataSnapshot.getKey(), dataSnapshot.getValue(QueueRequest.class));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                listener.onRequestChanged(dataSnapshot.getKey(), dataSnapshot.getValue(QueueRequest.class));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                listener.onRequestRemoved(dataSnapshot.getKey(), dataSnapshot.getValue(QueueRequest.class));
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void editRequest(QueueRequest request) throws TooLateException {
        Calendar now = Calendar.getInstance();
        Calendar requestTime = Calendar.getInstance();
        requestTime.setTime(request.getStartTime());
        long diff = requestTime.getTimeInMillis() - now.getTimeInMillis();
        //differenza in ore
        float h = diff / 1000 / 60 / 60;
        if(h < MAX_HOUR_EDIT){
            throw new TooLateException();
        }else{
            updateRequest(request);
        }
    }

    public void startRequest(QueueRequest request){
        if(request.getQueue() < 0){
            throw new IllegalArgumentException("Non sono state impostate le persone in coda");
        }
        if(request.getQueue() == 0) {
            this.endQueue(request);
        }else{
            request.setStatus(QueueRequestStatus.ACTIVE);
            updateRequest(request);
        }
    }

    public void endQueue(QueueRequest request) {
        request.setStatus(QueueRequestStatus.WAITING_FOR_CLOSE);
        updateRequest(request);
    }

    public void removeHelper(QueueRequest request) throws TooLateException {
        User h = request.getHelper();
        QueueRequestStatus status = request.getStatus();
        try{
            request.setHelper(null);
            request.setStatus(QueueRequestStatus.NO_HELPER_SELECTED);
            editRequest(request);
            database.child("helpers_queue_requests").child(h.getId()).child(request.getId()).removeValue();
        }catch(TooLateException e){
            request.setStatus(status);
            request.setHelper(h);
            throw e;
        }
    }

    public void subctratPersonRequest(QueueRequest request){
        request.setQueue(request.getQueue() - 1);
        if(request.getQueue() <= 0){
            endQueue(request);
        }else{
            updateRequest(request);
        }
    }

    private void updateRequest(QueueRequest request) {
        if(request.getHelper()!=null) {
            database.child("helpers_queue_requests").child(request.getHelper().getId()).child(request.getId()).setValue(request);
        }
        database.child("users_queue_requests").child(request.getUser().getId()).child(request.getId()).setValue(request);
    }

    public void removeRequest(QueueRequest request) throws TooLateException{
        if(request.getHelper()!=null) {
            database.child("helpers_queue_requests").child(request.getHelper().getId()).child(request.getId()).removeValue();
        }
        database.child("users_queue_requests").child(request.getUser().getId()).child(request.getId()).removeValue();
    }

    public void closeRequestForHelper(QueueRequest request) {
        database.child("helpers_queue_requests").child(request.getHelper().getId()).child(request.getId()).removeValue();
    }

    public void closeRequestForKiuer(QueueRequest request) {
        database.child("users_queue_requests").child(request.getUser().getId()).child(request.getId()).removeValue();
    }

    public interface RequestChangedListener {
        void onRequestRead(Map<String, QueueRequest> requests);
        void onRequestChanged(String key, QueueRequest value);
        void onRequestAdded(String key, QueueRequest value);
        void onRequestRemoved(String key, QueueRequest value);
    }
}
