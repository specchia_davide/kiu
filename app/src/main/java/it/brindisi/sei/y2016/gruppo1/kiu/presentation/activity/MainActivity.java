package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.AuthResult;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.UserPreferences;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentForgottPass;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentLogin;
import it.brindisi.sei.y2016.gruppo1.kiu.service.HelperService;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.Constants;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

public class MainActivity extends AppCompatActivity implements FragmentLogin.LoginListener {
    public static final String LAST_LOGIN_EMAIL = "lastLoginEmail";
    ApplicationServiceUser asu;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        asu = new ApplicationServiceUser();
        if (asu.getLoggedUser() != null) {
            startNotificationService();
            openHomeActivity();
        }else {
            setContentView(R.layout.activity_main);
            FragmentLogin fragmentLogin = new FragmentLogin();
            fragmentLogin.setListener(this);
            String lastLoginEmail = loadEmail();
            if (lastLoginEmail != null) {
                fragmentLogin.setLastLoginEmail(lastLoginEmail);
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, fragmentLogin).commit();
        }
    }

    private void startNotificationService() {
        Intent service = new Intent(this, HelperService.class);
        startService(service);
    }

    @Override
    public void onLogin(final String email, String password) {
        if (isNetworkConnected()) {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage(getString(R.string.attendere));
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();

            try {
                asu.login(email, password, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(boolean success, AuthResult task) {
                        if (success) {
                            saveEmail(email);
                            openHomeActivity();
                        } else {
                            if (progressDialog != null) {
                                progressDialog.dismiss();
                            }
                            Crouton.makeText(MainActivity.this, R.string.email_pass_errata, Style.ALERT).show();
                        }
                    }
                });
            } catch (IllegalArgumentException e) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Crouton.makeText(MainActivity.this, R.string.email_pass_vuote, Style.ALERT).show();
            }
        } else {
            Crouton.makeText(MainActivity.this, "Connessione non Trovata", Style.ALERT, R.id.search_view_item).show();
        }
    }

    private void openHomeActivity() {
        asu.getUserPreferences(new OnCompleteListener<UserPreferences>() {
            @Override
            public void onComplete(boolean success, UserPreferences result) {
                Class homeClass;
                if(success){
                    if (result.isKiuerHome()) {
                        homeClass = KiuerHomeActivity.class;
                    } else {
                        homeClass = HelperHomeActivity.class;
                    }
                }else{
                    result = new UserPreferences();
                    result.setKiuerHome(true);
                    asu.updateUserPreferences(result);
                    homeClass = KiuerHomeActivity.class;
                }
                Intent home = new Intent(MainActivity.this, homeClass);
                home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(home);
            }
        });
    }

    @Override
    public void onForgottenPassword(String email) {
        Fragment forgpass = new FragmentForgottPass();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, forgpass).addToBackStack(null).commit();
    }

    @Override
    public void onRegistration() {
        Intent registerIntent = new Intent(this, RegistrationActivity.class);
        startActivity(registerIntent);
    }

    private void saveEmail(String email) {
        SharedPreferences sp = getSharedPreferences(Constants.SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(LAST_LOGIN_EMAIL, email);
        editor.apply();
    }

    private String loadEmail() {
        SharedPreferences sp = getSharedPreferences(Constants.SETTINGS, MODE_PRIVATE);
        return sp.getString(LAST_LOGIN_EMAIL, null);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
