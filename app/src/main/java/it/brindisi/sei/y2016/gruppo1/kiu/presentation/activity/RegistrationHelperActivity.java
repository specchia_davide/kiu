package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import java.util.Calendar;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceHelper;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.HelperSettings;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Location;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.UserPreferences;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentSelectArea;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentSettingHelper;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.Constants;

/**
 * Created by Davide on 30/06/2016.
 */
public class RegistrationHelperActivity extends AppCompatActivity implements FragmentSelectArea.Listener, FragmentSettingHelper.Listener {
    private FragmentSelectArea fragmentSelectArea;
    private FragmentSettingHelper fragmentSettingHelper;
    private Location location;
    private int radius;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("selectArea", fragmentSelectArea.isAdded());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        fragmentSelectArea = new FragmentSelectArea();
        fragmentSelectArea.setListener(this);

        fragmentSettingHelper = new FragmentSettingHelper();
        fragmentSettingHelper.setListener(this);

        FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
        if(savedInstanceState == null || savedInstanceState.getBoolean("selectArea")) {
            fragmentManager.replace(R.id.container, fragmentSelectArea).commit();
        }else{
            fragmentManager.replace(R.id.container, fragmentSettingHelper).commit();
        }
    }

    @Override
    public void onNext(Location location, int radius) {
        this.location = location;
        this.radius = radius;
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragmentSettingHelper)
                .addToBackStack(null).commit();
    }

    @Override
    public void onComplete(boolean[] days, Calendar startTime, Calendar endDate, int fare) {
        HelperSettings settings = new HelperSettings();
        settings.setFare(fare);
        settings.setStartTime(startTime.getTime());
        settings.setEndTime(endDate.getTime());
        settings.setCenter(location);
        settings.setRadius(radius);
        for(int i = 0; i < days.length; i++){
            if(days[i])
                settings.addDay(i);
        }

        ApplicationServiceUser asu = new ApplicationServiceUser();

        ApplicationServiceHelper ash = new ApplicationServiceHelper();
        ash.updateHelperSettings(asu.getLoggedUser(), settings);

        UserPreferences up = new UserPreferences();
        up.setKiuerHome(false);
        asu.updateUserPreferences(up);

        Intent home = new Intent(this, HelperHomeActivity.class);
        home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(home);
    }
}
