package it.brindisi.sei.y2016.gruppo1.kiu.business.model;

/**
 * Rappresenta lo stato della {@link QueueRequestStatus}.
 * L'ordine dovrebbe essere
 * <li>{@link #WAITING_FOR_QUEUE}</li>
 * <li>{@link #ACTIVE}</li>
 * <li>{@link #WAITING_FOR_CLOSE}</li>
 * <li>{@link #CLOSED}</li>
 * <li>{@link #NO_HELPER_SELECTED}</li>
 */
public enum QueueRequestStatus {
    /**
     * Rappresenta lo stato iniziale della richiesta, l'Helper è stato selezionato al momento della creazione
     * e si sta aspettando l'inizio effettivo della coda
     */
    WAITING_FOR_QUEUE,
    /**
     * Rappresenta lo stato in cui l'Helper ha effettivamente iniziato la coda
     */
    ACTIVE,
    /**
     * Rappresenta lo stato in cui l'Helper ha concluso la coda, si attende la chiusura della stessa da parte del Kiuer
     */
    WAITING_FOR_CLOSE,
    /**
     * La coda si considera conclusa nel momento in cui il Kiuer la chiude
     */
    CLOSED,
    /**
     * Stato in cui l'Helper non ha accettato la richiesta del Kiuer oppure si è ritirato in un secondo momento
     */
    NO_HELPER_SELECTED
}
