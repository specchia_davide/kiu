package it.brindisi.sei.y2016.gruppo1.kiu.business.model;

import java.io.Serializable;

public class Location implements Serializable {
    private double lat;
    private double lng;

    public Location(android.location.Location location) {
        this.lat = location.getLatitude();
        this.lng = location.getLongitude();
    }

    public Location() { }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public float distanceTo(Location l) {
        android.location.Location l1 = new android.location.Location("");
        l1.setLatitude(lat);
        l1.setLongitude(lng);
        android.location.Location l2 = new android.location.Location("");
        l2.setLatitude(l.lat);
        l2.setLongitude(l.lng);

        return l1.distanceTo(l2);
    }
}