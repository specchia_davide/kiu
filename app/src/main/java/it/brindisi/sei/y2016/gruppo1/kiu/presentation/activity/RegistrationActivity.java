package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import java.io.IOException;
import java.io.InputStream;
import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.UserPreferences;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentRegistrationFirstStep;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentRegistrationSecondStep;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.Constants;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.ImagePicker;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by Matteo on 27/05/2016.
 */
public class RegistrationActivity extends AppCompatActivity implements FragmentRegistrationFirstStep.Listener, FragmentRegistrationSecondStep.RegistrationSecondStepListener {
    private static final int REQUEST_CODE = 1;
    private static final String LAST_LOGIN_EMAIL = "lastLoginEmail";
    public static final String SETTINGS = "settings";
    private String firstName, lastName, email, password;
    private Bitmap profilePicture;

    FragmentRegistrationFirstStep firstStepFragment;
    FragmentRegistrationSecondStep secondStepFragment;

    ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        if(firstStepFragment ==null)
        firstStepFragment = new FragmentRegistrationFirstStep();
        firstStepFragment.setListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        secondStepFragment = new FragmentRegistrationSecondStep();
        secondStepFragment.setListener(this);
        FragmentManager fm = getSupportFragmentManager();

        if(savedInstanceState == null || savedInstanceState.getBoolean("first", true)){
            fm.beginTransaction().replace(R.id.container, firstStepFragment).commit();
        }else{
            fm.beginTransaction().replace(R.id.container, secondStepFragment).commit();
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("first", firstStepFragment.isAdded());

    }

    @Override
    public void onNext(String firstName, String lastName, final String email, String password, Bitmap picture) {
        ApplicationServiceUser aso = new ApplicationServiceUser();
        progress = new ProgressDialog(RegistrationActivity.this);
        progress.setMessage(getString(R.string.attendere));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
        aso.register(email, password, firstName + " " + lastName, picture, new OnCompleteListener<User>() {
            @Override
            public void onComplete(boolean success, User result) {
                progress.dismiss();
                if(success){
                    saveEmail(email);
                    //A registrazione effettuata viene effettuato automaticamente il login
                    FragmentManager fm = getSupportFragmentManager();
                    fm.beginTransaction().replace(R.id.container, secondStepFragment).commit();
                }else{
                    final AlertDialog.Builder miaAlert = new AlertDialog.Builder(RegistrationActivity.this);
                    miaAlert.setMessage(R.string.email_in_uso);
                    miaAlert.setTitle(R.string.registrazione_fallita);
                    miaAlert.setCancelable(false);
                    miaAlert.setPositiveButton(android.R.string.ok, null);

                    AlertDialog alert = miaAlert.create();
                    alert.show();
                }
            }
        });
    }

    @Override
    public void onPickPicture() {
        Intent intent = ImagePicker.getPickImageIntent(this);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            if (profilePicture != null) {
                profilePicture.recycle();
            }

            Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
            profilePicture=bitmap;
            CropCircleTransformation cropper = new CropCircleTransformation();
            firstStepFragment.setProfilePicture(cropper.transform(bitmap));

        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private void saveEmail(String email){
        SharedPreferences sp = getSharedPreferences(SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(LAST_LOGIN_EMAIL, email);
        editor.apply();
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;


            case R.id.action_cart:
                if(firstStepFragment.checkInformations()) {
                    String firstName = firstStepFragment.getFirstName();
                    String lastName = firstStepFragment.getLastName();
                    String email = firstStepFragment.getEmail();
                    String password = firstStepFragment.getPassword();
                    Bitmap profilePicture = firstStepFragment.getProfilePicture();
                    this.onNext(firstName, lastName, email, password, profilePicture);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openHomeKiuer() {
        UserPreferences up = new UserPreferences();
        up.setKiuerHome(true);
        ApplicationServiceUser asu = new ApplicationServiceUser();
        asu.updateUserPreferences(up);
        Intent home = new Intent(this, KiuerHomeActivity.class);
        home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(home);
    }

    public void openHomeHelper(){
        Intent home = new Intent(this, RegistrationHelperActivity.class);
        //home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(home);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.registration_menu, menu);
        return true;
    }

    public void onBackPressed() {
        if(secondStepFragment.isVisible())
            secondStepFragment.onBackPressed();
        else
            super.onBackPressed();
    }

}

