package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.brindisi.sei.y2016.gruppo1.kiu.R;

/**
 * Created by degiu on 07/07/2016.
 */
public class FragmentSettingsHelper extends android.support.v4.app.Fragment{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings_helper, container, false);
        return v;
    }
}
