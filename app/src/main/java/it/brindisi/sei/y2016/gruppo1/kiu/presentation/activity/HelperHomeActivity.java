package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dexafree.materialList.card.provider.ListCardProvider;
import com.google.gson.Gson;

import java.util.Map;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceQueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter.QueueRequestAdapter;

/**
 * Created by Davide on 02/07/2016.
 */
public class HelperHomeActivity extends NavigationActivity implements AdapterView.OnItemClickListener {

    QueueRequestAdapter request;
    //QueueRequest queueRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helper_home);

        final ListView listView = (ListView)findViewById(R.id.listView);
        listView.setOnItemClickListener(this);

        request = new QueueRequestAdapter(this);

        ApplicationServiceQueueRequest asqr = new ApplicationServiceQueueRequest();
        asqr.setHelperRequestChangedListener(new ApplicationServiceQueueRequest.RequestChangedListener() {
            @Override
            public void onRequestRead(Map<String, QueueRequest> requests) {
                if(requests.size() > 0) {
                    for (String key : requests.keySet()) {
                        request.putItem(key, requests.get(key));
                    }
                    listView.setAdapter(request);

                }
            }

            @Override
            public void onRequestChanged(String key, QueueRequest value) {
                request.putItem(key, value);
                listView.setAdapter(request);
            }

            @Override
            public void onRequestAdded(String key, QueueRequest value) {
                request.putItem(key, value);
                listView.setAdapter(request);
            }

            @Override
            public void onRequestRemoved(String key, QueueRequest value) {
                request.removeItem(key);
                listView.setAdapter(request);
            }
        });

        View v = LayoutInflater.from(this).inflate(R.layout.empty_result_list_layout, null);
        TextView tx = (TextView) v.findViewById(R.id.textView) ;
        tx.setText(R.string.no_request);
        ((ViewGroup)findViewById(R.id.container)).addView(v);
        listView.setEmptyView(v);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Gson gson = new Gson();
        Intent intent = new Intent(this, HelperRequestDetailsActivity.class);
        intent.putExtra("request", gson.toJson(request.getItem(position)));
        startActivity(intent);
    }


}
