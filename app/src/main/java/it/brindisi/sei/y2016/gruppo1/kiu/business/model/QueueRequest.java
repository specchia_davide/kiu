package it.brindisi.sei.y2016.gruppo1.kiu.business.model;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by danieles on 21/06/16.
 */
public class QueueRequest implements Serializable{
    private User user;
    private Place place;
    private Date startTime;
    private Date lastEdit;
    private QueueRequestStatus status = QueueRequestStatus.WAITING_FOR_QUEUE;
    private int maxFare;  //La tariffa viene salvata come intero per evitare problemi di arrotondamento, sará quindi necessario dividerla per 100
    private String note; //Eventuali informazioni aggiuntive
    private User selectedHelper;
    private String id;
    private int queue;

    public QueueRequest() {
        this.lastEdit = Calendar.getInstance().getTime();
    }


    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public int getMaxFare() {
        return maxFare;
    }

    public void setMaxFare(int maxFare) {
        this.maxFare = maxFare;
    }

    public Date getLastEdit() {
        return lastEdit;
    }

    public void setLastEdit(Date lastEdit) {
        this.lastEdit = lastEdit;
    }

    @Exclude
    public QueueRequestStatus getStatus() {
        return status;
    }

    public void setStatus(QueueRequestStatus status) {
        this.status = status;
    }

    public String getStatusValue(){
        return this.status.name();
    }

    public void setStatusValue(String status) {
        if(status == null)
            this.status = null;
        else
            this.status = QueueRequestStatus.valueOf(status);
    }

    public User getHelper() {
        return selectedHelper;
    }

    public void setHelper(User selectedHelper) {
        this.selectedHelper = selectedHelper;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }
}
