package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.net.ConnectivityManagerCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServicePlace;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Place;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.PlaceResult;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter.SearchResultsAdapter;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

/**
 * Created by Davide on 08/06/2016.
 */
public class FragmentSearchResults extends ListFragment implements OnCompleteListener<PlaceResult> {

    private OnPlaceSelected listener;
    private Location location;

    public void setListener(OnPlaceSelected listener) {
        this.listener = listener;
    }

    public interface OnPlaceSelected{
        void onPlaceSelected(Place place);
    }
    PlaceResult placeResult;

    public void setQuery(String query) {
        ApplicationServicePlace asp = new ApplicationServicePlace();
        asp.searchPlace(query, location, this);
    }

    @Override
    public void onComplete(boolean success, PlaceResult result) {
        if(success){
            this.placeResult = result;
            if(location != null) {
                it.brindisi.sei.y2016.gruppo1.kiu.business.model.Location l = new it.brindisi.sei.y2016.gruppo1.kiu.business.model.Location(location);
                this.setListAdapter(new SearchResultsAdapter(result, l, getActivity()));
            }else{
                this.setListAdapter(new SearchResultsAdapter(result, null, getActivity()));
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if(this.listener != null)
            this.listener.onPlaceSelected(placeResult.getResults().get(position));
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        View v = getLayoutInflater(savedInstanceState).inflate(R.layout.empty_result_list_layout, null);
        ((ViewGroup)getListView().getParent()).addView(v);
        this.getListView().setEmptyView(v);
    }

    public void setLocation(Location s){
        this.location = s;
    }

}