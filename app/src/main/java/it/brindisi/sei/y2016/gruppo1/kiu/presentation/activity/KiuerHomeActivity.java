package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.lapism.searchview.SearchView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Place;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentHome;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentSearchResults;

public class KiuerHomeActivity extends NavigationActivity implements FragmentSearchResults.OnPlaceSelected, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, FragmentHome.Listener {
    private static final int GPS_PERMISSION_CODE = 1;

    FragmentHome fragmentHome;
    private FragmentSearchResults fragmentSearchResults;
    private SearchView searchView;
    private GoogleApiClient mGoogleApiClient;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        fragmentHome = new FragmentHome();
        fragmentHome.setListener(this);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.container, fragmentHome).commit();

        //Rimuovo l'actionbar (sostituita dalla toolbar)
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initSearchView();

        getDrawerLayout().addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                if (searchView != null && searchView.isSearchOpen()) {
                    searchView.close(true);
                }
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initSearchView() {
        searchView = (SearchView) findViewById(R.id.searchView);
        if (searchView != null) {
            searchView.setHint(getString(R.string.search_query_hint));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    if(isNetworkConnected()!=false) {
                        searchView.close(true);
                        updateQuery(query);
                        return true;
                    }
                    else {
                        Crouton.makeText(KiuerHomeActivity.this, "Connessione non Trovata.", Style.ALERT, R.id.search_view_item).show();
                        return false;
                    }
                }
            });
        }
        searchView.setOnMenuClickListener(new SearchView.OnMenuClickListener() {
            @Override
            public void onMenuClick() {
                getDrawerLayout().openDrawer(GravityCompat.START);
            }
        });
    }

    private void updateQuery(String query) {
        FragmentManager fm = getSupportFragmentManager();
        if (fragmentSearchResults != null && fragmentSearchResults.isAdded()) {
            fm.popBackStackImmediate();
        }
        fragmentSearchResults = new FragmentSearchResults();
        fragmentSearchResults.setListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fragmentSearchResults.setLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
        }
        fragmentSearchResults.setQuery(query);
        fm.beginTransaction().replace(R.id.container, fragmentSearchResults).addToBackStack(null).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                getDrawerLayout().openDrawer(GravityCompat.START);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (getDrawerLayout() != null && getDrawerLayout().isDrawerOpen(GravityCompat.START)) {
            getDrawerLayout().closeDrawer(GravityCompat.START);
        } else if (searchView != null && searchView.isSearchOpen()) {
            searchView.close(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPlaceSelected(Place place) {
        Intent intent = new Intent(this, ResultDetailsActivity.class);
        intent.putExtra("place", place);
        startActivity(intent);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    protected void onStart() {
        mGoogleApiClient.connect();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, GPS_PERMISSION_CODE);
        }

        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onQueueRequestSelected(QueueRequest queueRequest) {
        Intent intent = new Intent(this, QueueRequestDetailsActivity.class);

        //Per qualche motivo gli oggetti recuperati tramite Gson (Place) sono troppo grandi per poter essere
        //passati in un intent. Quindi riconverto l'oggetto in una stringa JSON.
        Gson gson = new Gson();
        intent.putExtra("request", gson.toJson(queueRequest));

        startActivity(intent);
    }
}
