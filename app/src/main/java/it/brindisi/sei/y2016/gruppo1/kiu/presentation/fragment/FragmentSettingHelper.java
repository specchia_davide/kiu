package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.blackcat.currencyedittext.CurrencyEditText;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import java.text.DateFormat;
import java.util.Calendar;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.HelperSettings;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.view.WeekButton;

/**
 * Created by Anonym on 01/07/2016.
 */
public class FragmentSettingHelper extends Fragment implements RadialTimePickerDialogFragment.OnTimeSetListener, View.OnClickListener {

    private static final String INIZIO_TIME_PICKER = "Inizio";
    private static final String FINE_TIME_PICKER = "Fine";
    private Calendar timeInizio=Calendar.getInstance();
    private Calendar timeFine=Calendar.getInstance();
    private EditText inizio;
    private EditText fine;
    private DateFormat df;
    private CurrencyEditText tariffa;
    private WeekButton[] days;
    private Listener listener;
    private HelperSettings hs;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting_helper, container, false);
        inizio = (EditText) v.findViewById(R.id.inizio);
        fine = (EditText) v.findViewById(R.id.fine);
        tariffa = (CurrencyEditText) v.findViewById(R.id.tariffa);
        ViewGroup dayToggles = (ViewGroup) v.findViewById(R.id.day_toggles);
        days = new WeekButton[dayToggles.getChildCount()];
        for(int i = 0; i < dayToggles.getChildCount(); i++){
            days[i] = (WeekButton) dayToggles.getChildAt(i);
        }

        df = DateFormat.getTimeInstance(DateFormat.SHORT);

        timeInizio.set(Calendar.HOUR_OF_DAY, 16);
        timeInizio.set(Calendar.MINUTE, 0);
        timeFine.set(Calendar.HOUR_OF_DAY, 23);
        timeFine.set(Calendar.MINUTE, 0);

        //Default
        inizio.setText(df.format(timeInizio.getTime()));
        fine.setText(df.format(timeFine.getTime()));
        tariffa.setText("1000");


        inizio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                        .setOnTimeSetListener(FragmentSettingHelper.this)
                        .setDoneText(getString(android.R.string.ok))
                        .setStartTime(timeInizio.get(Calendar.HOUR), timeInizio.get(Calendar.MINUTE))
                        .setCancelText(getString(android.R.string.cancel));
                rtpd.show(getFragmentManager(), INIZIO_TIME_PICKER);
            }

        });

        fine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                        .setOnTimeSetListener(FragmentSettingHelper.this)
                        .setDoneText(getString(android.R.string.ok))
                         .setStartTime(timeFine.get(Calendar.HOUR), timeFine.get(Calendar.MINUTE))
                        .setCancelText(getString(android.R.string.cancel));
                rtpd.show(getFragmentManager(), FINE_TIME_PICKER);
            }

        });

        Button next = (Button) v.findViewById(R.id.next);
        next.setOnClickListener(this);
        if(hs != null){
            Calendar temp = Calendar.getInstance();
            temp.setTime(hs.getStartTime());
            setStartTime(temp.get(Calendar.HOUR_OF_DAY), temp.get(Calendar.MINUTE));
            temp.setTime(hs.getEndTime());
            setEndTime(temp.get(Calendar.HOUR_OF_DAY), temp.get(Calendar.MINUTE));
            tariffa.setText(""+hs.getFare());
            for(int i = 0; i < 7; i++){
                days[i].setChecked(hs.hasDay(i));
            }
        }
        return v;
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        if(dialog.getTag().equals(INIZIO_TIME_PICKER)){
            setStartTime(hourOfDay, minute);
        }else{
            setEndTime(hourOfDay, minute);
        }
    }

    private void setEndTime(int hourOfDay, int minute) {
        timeFine.set(Calendar.HOUR_OF_DAY, hourOfDay);
        timeFine.set(Calendar.MINUTE, minute);
        fine.setText(df.format(timeFine.getTime()));
    }

    private void setStartTime(int hourOfDay, int minute) {
        timeInizio.set(Calendar.HOUR_OF_DAY, hourOfDay);
        timeInizio.set(Calendar.MINUTE, minute);
        inizio.setText(df.format(timeInizio.getTime()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.next:
                boolean[] selectedDays = getSelectedDays();
                if(checkInformations(selectedDays)) {
                    listener.onComplete(getSelectedDays(), this.timeInizio, this.timeFine, (int) tariffa.getRawValue());
                }else{
                    Crouton.makeText(getActivity(), getString(R.string.select_day_of_week), Style.ALERT).show();
                }
                break;
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private boolean checkInformations(boolean[] selectedDays) {
        boolean valid = false;
        for (boolean selectedDay : selectedDays) {

            valid = valid || selectedDay;
        }
        return valid;
    }

    private boolean[] getSelectedDays() {
        boolean[] selectedDays = new boolean[days.length];
        for(int i = 0; i < days.length; i++){
            selectedDays[i] = days[i].isChecked();
        }
        return selectedDays;
    }

    public void setDefault(HelperSettings helperSettings) {
        hs = helperSettings;
    }

    public interface Listener {
        void onComplete(boolean[] days, Calendar startTime, Calendar endDate, int fare);
    }
}
