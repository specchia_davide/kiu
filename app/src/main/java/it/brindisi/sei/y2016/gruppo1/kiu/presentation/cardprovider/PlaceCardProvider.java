package it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.dexafree.materialList.card.Action;
import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Place;

/**
 * Created by Davide on 03/07/2016.
 */
public class PlaceCardProvider extends CardProvider<PlaceCardProvider> implements OnMapReadyCallback, View.OnClickListener {
    public Place getPlace() {
        return place;
    }

    public PlaceCardProvider setPlace(Place mPlace) {
        this.place = mPlace;
        return this;
    }

    private Place place;

    @Override
    protected void onCreated() {
        super.onCreated();
        setLayout(R.layout.card_place_info);
    }

    @Override
    public void render(@NonNull View view, @NonNull Card card) {
//        super.render(view, card);
        if(place != null){
            TextView name = (TextView) view.findViewById(R.id.place_name);
            name.setText(place.getName());
            TextView address = (TextView) view.findViewById(R.id.address);
            address.setText(place.getFormattedAddress());
            SupportMapFragment mapFragment = new SupportMapFragment();
            mapFragment.getMapAsync(this);
            ((AppCompatActivity)getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
            View startNavigator = view.findViewById(R.id.start_navigator);
            startNavigator.setOnClickListener(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        MarkerOptions marker = new MarkerOptions();
        marker.position(new LatLng(place.getLocation().getLat(), place.getLocation().getLng()));
        googleMap.addMarker(marker);
        CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15);
        googleMap.moveCamera(camera);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.start_navigator:
                Uri navigatorUri = Uri.parse("google.navigation:q="+place.getLocation().getLat()+","+place.getLocation().getLng());
                Intent intent = new Intent(Intent.ACTION_VIEW, navigatorUri);
                intent.setPackage("com.google.android.apps.maps");
                getContext().startActivity(intent);
        }
    }
}
