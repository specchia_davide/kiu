package it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by degiu on 21/06/2016.
 */
public class AttivitaNeiDintorniAdapter extends BaseAdapter{
    String [] placeResult;
    private Context context;

    public AttivitaNeiDintorniAdapter(Context context){
        this.context=context;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent){
        if(v==null){
            v = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_2, null);
        }
        TextView name = (TextView) v.findViewById(android.R.id.text1);
        name.setText(null);
        return v;
    }
}
