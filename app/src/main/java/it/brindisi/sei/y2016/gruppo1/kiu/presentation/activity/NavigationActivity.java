package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Activity contenente il NavigationDrawer
 * Created by Davide on 27/06/2016.
 */
public abstract class NavigationActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private NavigationView navView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.navigation_drawer_base_layout);
        initNavigationDrawer();
    }

    private void initNavigationDrawer(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ApplicationServiceUser asu = new ApplicationServiceUser();
        User user = asu.getLoggedUser();
        navView = (NavigationView) findViewById(R.id.nav_view);
        View navHeader = navView.getHeaderView(0);
        TextView name = (TextView) navHeader.findViewById(R.id.nome);
        name.setText(user.getName());
        TextView email = (TextView) navHeader.findViewById(R.id.email);
        email.setText(user.getEmail());
        ImageView profilePic = (ImageView) navHeader.findViewById(R.id.profilePic);
        Picasso.with(this).load(user.getPhotoUrl()).placeholder(R.drawable.user_avatar)
                .transform(new CropCircleTransformation()).into(profilePic);
        getNavigationView().setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.logout:
                ApplicationServiceUser asu = new ApplicationServiceUser();
                asu.logout();
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.settings:
                Intent settings_intent= new Intent(this, SettingsActivity.class);
                startActivity(settings_intent);
                break;
        }
        getDrawerLayout().closeDrawer(GravityCompat.START);
        return true;
    }

    public NavigationView getNavigationView() {
        return navView;
    }

    protected DrawerLayout getDrawerLayout(){
        return this.drawerLayout;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        FrameLayout container = (FrameLayout)findViewById(R.id.layout_container);
        LayoutInflater li = LayoutInflater.from(this);
        View v = li.inflate(layoutResID, container, false);
        container.addView(v);
    }
}
