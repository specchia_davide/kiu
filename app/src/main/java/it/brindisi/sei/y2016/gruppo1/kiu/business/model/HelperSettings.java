package it.brindisi.sei.y2016.gruppo1.kiu.business.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Davide on 01/07/2016.
 */
public class HelperSettings implements Serializable {
    private Location center;
    private Date startTime;
    private int radius;
    private Date endTime;
    private int days;
    private int fare;

    public Location getCenter() {
        return center;
    }

    public void setCenter(Location center) {
        this.center = center;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public void addDay(int day){
        this.days |= (int)Math.pow(2, day);
    }

    public void removeDay(int day){
        this.days &= 0xFF - (int)Math.pow(2, day);
    }

    public boolean hasDay(int day){
        int tmp = this.days >> day;
        tmp &= 1;
        return tmp == 1;
    }

    public int getFare() {
        return fare;
    }

    public void setFare(int fare) {
        this.fare = fare;
    }
}
