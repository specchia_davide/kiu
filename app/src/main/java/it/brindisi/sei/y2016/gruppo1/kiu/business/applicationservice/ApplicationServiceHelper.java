package it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice;

import android.net.Uri;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import it.brindisi.sei.y2016.gruppo1.kiu.business.model.HelperSettings;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Location;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

/**
 * Created by Davide on 01/07/2016.
 */
public class ApplicationServiceHelper {

    private final DatabaseReference database;

    public ApplicationServiceHelper(){
        database = FirebaseDatabase.getInstance().getReference();
    }

    public void updateHelperSettings(User user, HelperSettings settings){
        database.child("helpers").child(user.getId()).setValue(settings);
        database.child("helpers").child(user.getId()).child("name").setValue(user.getName());
        database.child("helpers").child(user.getId()).child("photoUrl").setValue(user.getPhotoUrl());
    }

    public void getSuitableHelpers(final QueueRequest request, final OnCompleteListener<List<User>> listener){
        database.child("helpers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<User> helpers = new LinkedList<>();
                for(DataSnapshot helper : dataSnapshot.getChildren()){
                    HelperSettings settings = helper.getValue(HelperSettings.class);
                    if(checkHelperSettings(settings, request)){
                        User h = new User();
                        h.setHelperSettings(settings);
                        h.setName((String) helper.child("name").getValue());
                        h.setId(helper.getKey());
                        if(helper.child("rating").getValue() != null){
                            float rating = helper.child("rating").child("rating").getValue(Float.class);
                            h.setHelperRating(Math.round(rating));
                        }
                        if(helper.child("photoUrl").getValue() != null)
                            h.setPhotoUrl(helper.child("photoUrl").getValue().toString());
                        helpers.add(h);
                    }
                }
                listener.onComplete(true, helpers);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean checkHelperSettings(HelperSettings settings, QueueRequest request) {
        //Controllo tariffa
        boolean valid = settings.getFare() <= request.getMaxFare();

        //Controllo che il punto sia nell'area dell'Helper
        valid = valid && settings.getCenter().distanceTo(request.getPlace().getLocation()) < settings.getRadius();

        //Controllo disponibilità
        Calendar currentTime = Calendar.getInstance();
        currentTime.setTime(request.getStartTime());
        int day = currentTime.get(Calendar.DAY_OF_WEEK);
        //Devo effettuare una trasformazione poiché i giorni della settimana in Calendar iniziano di domenica con id 1;
        day -= 2;
        if(day == -1){
            day = 6;
        }
        valid = valid && settings.hasDay(day);
        //Controllo orari
        Calendar startTime = Calendar.getInstance();
        startTime.setTime(settings.getStartTime());
        startTime.set(Calendar.DAY_OF_MONTH, currentTime.get(Calendar.DAY_OF_MONTH));
        startTime.set(Calendar.MONTH, currentTime.get(Calendar.MONTH));
        startTime.set(Calendar.YEAR, currentTime.get(Calendar.YEAR));
        Calendar endTime = Calendar.getInstance();
        endTime.setTime(settings.getEndTime());
        endTime.set(Calendar.DAY_OF_MONTH, currentTime.get(Calendar.DAY_OF_MONTH));
        endTime.set(Calendar.MONTH, currentTime.get(Calendar.MONTH));
        endTime.set(Calendar.YEAR, currentTime.get(Calendar.YEAR));
        if(endTime.before(startTime)){
            endTime.add(Calendar.DAY_OF_MONTH, 1);
        }
        valid = valid && (currentTime.before(endTime) && currentTime.after(startTime));

        return valid;
    }

    public void assignRequest(QueueRequest request) {
        User h = request.getHelper();
        String key = database.child("helpers").child(h.getId()).child("assigned_to").push().getKey();
        database.child("helpers").child(h.getId()).child("assigned_to").child(key).setValue(request);
    }

    public void getHelperSettings(User utente, final OnCompleteListener<HelperSettings> onCompleteListener) {
        database.child("helpers").child(utente.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HelperSettings hs = dataSnapshot.getValue(HelperSettings.class);
                if(hs!=null){
                    onCompleteListener.onComplete(true, hs);

                }else{
                    onCompleteListener.onComplete(false, null);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}