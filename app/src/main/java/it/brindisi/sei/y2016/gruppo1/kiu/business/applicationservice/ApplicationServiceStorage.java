package it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

/**
 * Created by Davide on 02/06/2016.
 */
public class ApplicationServiceStorage {
    private final FirebaseStorage mStorage;
    private final StorageReference rootRef;

    public ApplicationServiceStorage(){
        this.mStorage = FirebaseStorage.getInstance();
        this.rootRef = mStorage.getReferenceFromUrl("gs://kiu-queue-search.appspot.com");
    }

    public void uploadFile(String path, byte[] data, OnSuccessListener<? super UploadTask.TaskSnapshot> onSuccessListener){
        StorageReference pathRef = rootRef.child(path);
        UploadTask task = pathRef.putBytes(data);
        if(onSuccessListener != null)
            task.addOnSuccessListener(onSuccessListener);
    }
}
