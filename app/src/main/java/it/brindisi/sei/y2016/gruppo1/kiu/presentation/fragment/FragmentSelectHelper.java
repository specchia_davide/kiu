package it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceHelper;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.adapter.HelperAdapter;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

/**
 * Created by Davide on 02/07/2016.
 */
public class FragmentSelectHelper extends ListFragment implements OnCompleteListener<List<User>> {

    private List<User> helpers;
    private Listener listener;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        View v = getLayoutInflater(savedInstanceState).inflate(R.layout.empty_result_list_layout, null);
        ((ViewGroup) getListView().getParent()).addView(v);
        this.getListView().setEmptyView(v);
    }

    public void setQueueRequest(QueueRequest request) {
        ApplicationServiceHelper ash = new ApplicationServiceHelper();
        ash.getSuitableHelpers(request, this);
    }

    @Override
    public void onComplete(boolean success, List<User> result) {
        if (success) {
            this.helpers = result;
            HelperAdapter adapter = new HelperAdapter(result, getActivity());
            this.setListAdapter(adapter);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        listener.onHelperSelected(helpers.get(position));
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener{
        void onHelperSelected(User h);
    }
}
