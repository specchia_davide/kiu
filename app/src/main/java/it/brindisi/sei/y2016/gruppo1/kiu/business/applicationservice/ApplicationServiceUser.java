package it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice;


import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.UserPreferences;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.UserRating;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

/**
 * Created by Davide on 31/05/2016.
 */
public class ApplicationServiceUser {
    private final FirebaseAuth mAuth;
    private DatabaseReference database;

    public ApplicationServiceUser() {
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();
    }

    public void register(String email, String password, final String name, final Bitmap picture, final OnCompleteListener<User> onCompleteListener) {
        Task<AuthResult> task = mAuth.createUserWithEmailAndPassword(email, password);
        task.addOnCompleteListener(new  com.google.android.gms.tasks.OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(Task task) {
                if (task.isSuccessful()) {
                    //Registrazione avvenuta con successo
                    //Aggiungo il nome ed eventualmente carico l'immagine di profilo
                    FirebaseUser user = mAuth.getCurrentUser();
                    UserProfileChangeRequest.Builder builder = new UserProfileChangeRequest.Builder();
                    builder.setDisplayName(name);
                    if (picture != null) {
                        uploadProfilePicture(user, picture);
                    }
                    user.updateProfile(builder.build());
                    onCompleteListener.onComplete(true, getLoggedUser());
                }else{
                    onCompleteListener.onComplete(false, null);
                }
            }
        });
    }

    private void uploadProfilePicture(final FirebaseUser user, Bitmap picture) {
        ApplicationServiceStorage ass = new ApplicationServiceStorage();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        picture.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        ass.uploadFile("user_images/" + user.getUid() + ".jpg", data, new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                UserProfileChangeRequest.Builder builder = new UserProfileChangeRequest.Builder();
                builder.setPhotoUri(taskSnapshot.getDownloadUrl());
                user.updateProfile(builder.build());
            }
        });
    }

    public void login(String email, String password, final OnCompleteListener<AuthResult> onCompleteListener) {
        Task<AuthResult> task = mAuth.signInWithEmailAndPassword(email, password);
        if (onCompleteListener != null)
            task.addOnCompleteListener(new com.google.android.gms.tasks.OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()) {
                        onCompleteListener.onComplete(task.isSuccessful(), task.getResult());
                    }else{
                        onCompleteListener.onComplete(false, null);
                    }
                }
            });
    }

    public void resetPassword(String email, final OnCompleteListener<Void> onCompleteListener) {
        Task<Void> task = mAuth.sendPasswordResetEmail(email);
        if (onCompleteListener != null)
            task.addOnCompleteListener(new com.google.android.gms.tasks.OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    onCompleteListener.onComplete(task.isSuccessful(), task.getResult());
                }
            });
    }

    public User getLoggedUser() {
        if (mAuth.getCurrentUser() == null) {
            return null;
        } else {
            User user = new User();
            user.setName(mAuth.getCurrentUser().getDisplayName());
            user.setEmail(mAuth.getCurrentUser().getEmail());
            if(mAuth.getCurrentUser().getPhotoUrl() != null)
                user.setPhotoUrl(mAuth.getCurrentUser().getPhotoUrl().toString());
            user.setId(mAuth.getCurrentUser().getUid());
            return user;
        }
    }

    public void logout() {
        mAuth.signOut();
    }

    public void getUserPreferences(final OnCompleteListener<UserPreferences> onCompleteListener){
        database.child("user_pref").child(getLoggedUser().getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserPreferences userPreferences = dataSnapshot.getValue(UserPreferences.class);
                if(userPreferences != null){
                    onCompleteListener.onComplete(true, userPreferences);
                }else{
                    onCompleteListener.onComplete(false, null);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void updateUserPreferences(UserPreferences userPreferences){
        database.child("user_pref").child(getLoggedUser().getId()).setValue(userPreferences);
    }

    public void leaveUserFeedback(final User userRated, final int progress, final OnCompleteListener<Void> onCompleteListener) {
        database.child("user_pref").child(userRated.getId()).child("rating").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserRating rating = dataSnapshot.getValue(UserRating.class);
                if(rating != null){
                    int numberOfFeedback = rating.getNumberOfFeedback();
                    float ratingValue = rating.getRating();
                    float newRating = ((ratingValue * numberOfFeedback) + progress) / (numberOfFeedback + 1);
                    rating.setNumberOfFeedback(numberOfFeedback + 1);
                    rating.setRating(newRating);
                    database.child("user_pref").child(userRated.getId()).child("rating").setValue(rating);
                }else{
                    rating = new UserRating();
                    rating.setRating(progress);
                    rating.setNumberOfFeedback(1);
                    database.child("user_pref").child(userRated.getId()).child("rating").setValue(rating);
                }
                onCompleteListener.onComplete(true, null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void leaveHelperFeedback(final User userRated, final int progress, final OnCompleteListener<Void> onCompleteListener) {
        database.child("helpers").child(userRated.getId()).child("rating").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserRating rating = dataSnapshot.getValue(UserRating.class);
                if(rating != null){
                    int numberOfFeedback = rating.getNumberOfFeedback();
                    float ratingValue = rating.getRating();
                    float newRating = ((ratingValue * numberOfFeedback) + progress) / (numberOfFeedback + 1);
                    rating.setNumberOfFeedback(numberOfFeedback + 1);
                    rating.setRating(newRating);
                    database.child("helpers").child(userRated.getId()).child("rating").setValue(rating);
                }else{
                    rating = new UserRating();
                    rating.setRating(progress);
                    rating.setNumberOfFeedback(1);
                    database.child("helpers").child(userRated.getId()).child("rating").setValue(rating);
                }
                onCompleteListener.onComplete(true, null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getUserRating(User user, final OnCompleteListener<UserRating> onCompleteListener){
        database.child("user_pref").child(user.getId()).child("rating").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserRating rating = dataSnapshot.getValue(UserRating.class);
                if(rating != null){
                    onCompleteListener.onComplete(true, rating);
                }else{
                    onCompleteListener.onComplete(false, null);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void updateUserInfo(String email, String nome, String cognome, Bitmap image, final OnCompleteListener<User> onCompleteListener) {
        FirebaseUser user = mAuth.getCurrentUser();
        UserProfileChangeRequest.Builder builder = new UserProfileChangeRequest.Builder();
        builder.setDisplayName(nome + " " + cognome);
        if (image != null) {
            Bitmap bitmap = image.copy(Bitmap.Config.ARGB_8888, false);
            uploadProfilePicture(user, bitmap);
        }
        user.updateEmail(email);
        user.updateProfile(builder.build()).addOnCompleteListener(new com.google.android.gms.tasks.OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                onCompleteListener.onComplete(true, getLoggedUser());
            }
        });
    }
}
