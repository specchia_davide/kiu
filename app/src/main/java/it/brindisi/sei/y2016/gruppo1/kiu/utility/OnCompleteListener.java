package it.brindisi.sei.y2016.gruppo1.kiu.utility;

public interface OnCompleteListener<T> {
    void onComplete(boolean success, T result);
}