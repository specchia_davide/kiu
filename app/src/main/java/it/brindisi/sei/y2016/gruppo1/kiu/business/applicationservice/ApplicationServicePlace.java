package it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice;

import android.location.Location;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.network.AsyncTaskNearbyPlace;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.PlaceResult;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.jsondeserializer.PlaceDeserializer;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.network.AsyncTaskSearchPlace;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Place;

/**
 * Created by Davide on 08/06/2016.
 */
public class ApplicationServicePlace {
    /**
     * Numero minimo di elementi di una ricerca delle attività vicine
     * senza che venga utilizzata la ricerca generica
     */
    private static final int RESULT_SIZE_TRESHOLD = 5;

    public void searchPlace(final String query, @Nullable Location location, final OnCompleteListener<PlaceResult> onCompleteListener) {
        if(location != null){
            searchPlaceLocation(query, location, new OnCompleteListener<PlaceResult>() {
                @Override
                public void onComplete(boolean success, final PlaceResult result) {
                    if(!(success && result.getResults().size() >= RESULT_SIZE_TRESHOLD)){
                        searchPlaceGeneric(query, new OnCompleteListener<PlaceResult>() {
                            @Override
                            public void onComplete(boolean success, PlaceResult r) {
                                if(success){
                                    result.mergeResults(r);
                                }
                                onCompleteListener.onComplete(success, result);
                            }
                        });
                    }else {
                        onCompleteListener.onComplete(success, result);
                    }
                }
            });
        }else{
            searchPlaceGeneric(query, new OnCompleteListener<PlaceResult>() {
                @Override
                public void onComplete(boolean success, PlaceResult result) {
                    if(success){
                        onCompleteListener.onComplete(success, result);
                    }
                }
            });
        }
    }

    private void searchPlaceGeneric(String query, final OnCompleteListener<PlaceResult> onCompleteListener) {
        AsyncTaskSearchPlace atsp = new AsyncTaskSearchPlace(new OnCompleteListener<String>() {
            @Override
            public void onComplete(boolean success, String result) {
                if(success){
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.registerTypeAdapter(Place.class, new PlaceDeserializer());
                    Gson gson = gsonBuilder.create();
                    PlaceResult placeResult = gson.fromJson(result, PlaceResult.class);
                    onCompleteListener.onComplete(true, placeResult);
                }
            }
        });
        atsp.execute(query);
    }

    private void searchPlaceLocation(String query, Location location, final OnCompleteListener<PlaceResult> onCompleteListener){
        AsyncTaskNearbyPlace atsp = new AsyncTaskNearbyPlace(new OnCompleteListener<String>() {
            @Override
            public void onComplete(boolean success, String result) {
                if(success){
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.registerTypeAdapter(Place.class, new PlaceDeserializer());
                    Gson gson = gsonBuilder.create();
                    PlaceResult placeResult = gson.fromJson(result, PlaceResult.class);
                    onCompleteListener.onComplete(true, placeResult);
                }
            }
        });
        String locationString = location.getLatitude()+","+location.getLongitude();
        atsp.execute(query, locationString);
    }
}
