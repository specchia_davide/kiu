package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;

import java.util.Calendar;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import it.brindisi.sei.y2016.gruppo1.kiu.R;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceHelper;
import it.brindisi.sei.y2016.gruppo1.kiu.business.applicationservice.ApplicationServiceUser;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.HelperSettings;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.Location;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentSelectArea;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentSettingHelper;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentSettingsKiuer;
import it.brindisi.sei.y2016.gruppo1.kiu.utility.OnCompleteListener;

public class SettingsActivity extends NavigationActivity implements FragmentSelectArea.Listener, FragmentSettingHelper.Listener, FragmentSettingsKiuer.Listener, OnCompleteListener<HelperSettings> {

    FragmentSettingsKiuer fragmentSettingsKiuer;
    private ApplicationServiceUser user = new ApplicationServiceUser();
    HelperSettings helperSettings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        fragmentSettingsKiuer = new FragmentSettingsKiuer();
        fragmentSettingsKiuer.setListener(this);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.container, fragmentSettingsKiuer).commit();

        ApplicationServiceHelper ash = new ApplicationServiceHelper();
        ash.getHelperSettings(user.getLoggedUser(), this);

    }
    public void onDispSettings() {
        FragmentSettingHelper fsa = new FragmentSettingHelper();
        fsa.setListener(this);
        fsa.setDefault(helperSettings);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fsa).addToBackStack(null).commit();
    }

    @Override
    public void onConfirm(String email, String name, String lastName, Bitmap picture) {
        ApplicationServiceHelper ash = new ApplicationServiceHelper();
        ash.updateHelperSettings(user.getLoggedUser(), helperSettings);
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.attendere));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
        user.updateUserInfo(email, name,lastName, picture, new OnCompleteListener<User>() {
            @Override
            public void onComplete(boolean success, User result) {
                Crouton.makeText(SettingsActivity.this, R.string.sett_update, Style.CONFIRM).show();
               finish();
            }
        });
    }

    public void onAreaSettings() {
        FragmentSelectArea fsa = new FragmentSelectArea();
        fsa.setListener(this);
        fsa.setDefault(helperSettings);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fsa).addToBackStack(null).commit();
    }
    /**
     * richiamato quando viene cambiata la posizione sulla mappa
     * @param location
     * @param radius
     */
    @Override
    public void onNext(Location location, int radius) {
        getSupportFragmentManager().popBackStackImmediate();
        helperSettings.setCenter(location);
        helperSettings.setRadius(radius);

    }

    /**
     * richiamato quando viene cambiata la disponibilità
     * @param days
     * @param startTime
     * @param endDate
     * @param fare
     */
    @Override
    public void onComplete(boolean[] days, Calendar startTime, Calendar endDate, int fare) {
        getSupportFragmentManager().popBackStackImmediate();
        helperSettings.setFare(fare);
        helperSettings.setStartTime(startTime.getTime());
        helperSettings.setEndTime(endDate.getTime());
        for(int i = 0; i < days.length; i++){
            if(days[i])
                helperSettings.addDay(i);
            else{
                helperSettings.removeDay(i);
            }
        }
    }

    @Override
    public void onComplete(boolean success, HelperSettings result) {
        if(success) {
            helperSettings = result;
            fragmentSettingsKiuer.isHelper();
        }
    }
}
