package it.brindisi.sei.y2016.gruppo1.kiu.presentation.cardprovider;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.View;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;

import it.brindisi.sei.y2016.gruppo1.kiu.R;

/**
 * Created by Matteo on 11/07/2016.
 */
public class HintCardProvider extends CardProvider<HintCardProvider>{

    @Override
    protected void onCreated() {
        super.onCreated();
        setLayout(R.layout.card_hint);
    }
    @Override
    public void render(@NonNull View view, @NonNull Card card) {
        super.render(view, card);
    }
}
