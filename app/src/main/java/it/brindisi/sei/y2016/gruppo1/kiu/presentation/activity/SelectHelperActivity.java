package it.brindisi.sei.y2016.gruppo1.kiu.presentation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;

import it.brindisi.sei.y2016.gruppo1.kiu.business.model.QueueRequest;
import it.brindisi.sei.y2016.gruppo1.kiu.business.model.User;
import it.brindisi.sei.y2016.gruppo1.kiu.presentation.fragment.FragmentSelectHelper;

/**
 * Created by Davide on 09/07/2016.
 */
public class SelectHelperActivity extends AppCompatActivity implements FragmentSelectHelper.Listener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Gson gson = new Gson();
        QueueRequest request = gson.fromJson(getIntent().getExtras().getString("request"), QueueRequest.class);
        FragmentSelectHelper fsh = new FragmentSelectHelper();
        fsh.setListener(this);
        fsh.setQueueRequest(request);

        //Aggiungo il fragment alla radice
        getSupportFragmentManager().beginTransaction().add(android.R.id.content, fsh).commit();
    }

    @Override
    public void onHelperSelected(User h) {
        Intent result = new Intent();
        result.putExtra("helper", h);
        setResult(RESULT_OK, result);
        finish();
    }
}
